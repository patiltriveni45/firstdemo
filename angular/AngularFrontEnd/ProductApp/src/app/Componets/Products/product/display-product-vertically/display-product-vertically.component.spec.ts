import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayProductVerticallyComponent } from './display-product-vertically.component';

describe('DisplayProductVerticallyComponent', () => {
  let component: DisplayProductVerticallyComponent;
  let fixture: ComponentFixture<DisplayProductVerticallyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayProductVerticallyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DisplayProductVerticallyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
