import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/Models/product';
import { ProductService } from 'src/app/Service/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  product?:Product[];
    constructor(private productservice:ProductService) {
  
     
     }
  
    ngOnInit(): void {
      this.productservice.GetAllProduct().subscribe(response=>{
        
        this.product=response;
        console.log(response);
        
        
      });
      
    }
  }  
