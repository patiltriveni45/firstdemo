import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/Models/product';

@Component({
  selector: 'app-display-product-horizontally',
  templateUrl: './display-product-horizontally.component.html',
  styleUrls: ['./display-product-horizontally.component.css']
})
export class DisplayProductHorizontallyComponent implements OnInit {
@Input('mydata')public products?:Product[]
  constructor() { }

  ngOnInit(): void {
  }

}
