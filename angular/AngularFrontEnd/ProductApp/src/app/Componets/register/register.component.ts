import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/Models/user';
import { UserService } from 'src/app/Service/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
 user:User
  constructor(private userservice:UserService ,private route:Router) { 
    console.log("Within Register Component");
    this.user=new User()
    
  }

  ngOnInit(): void {
    // this.registerUser()
  }
  registerUser() {
    // this.user.Name="Rinku";
    // this.user.Email="Rinku@gmail.com";
    // this.user.Password="Rinku@123";
    this.user.IsBlock=false
    console.log(this.user);
    
    this.userservice.RegisterUser(this.user).subscribe(res=>{
      if(res){
        console.log("Regitser Success");
        Swal.fire(
          'User Resgistration',
          'Registration Success',
          'success'
        )
        this.route.navigate(['login'])
      }
      else{
        console.log("Registration Faild");
        
      }
    })
  }

}
