import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private authservice:AuthService) { }
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // let authservice=this.injector.get(AuthService)
    let tokenizereq=req.clone({
      setHeaders:{
        Authorization:`Bearer ${this.authservice.gettoken()}`
      }
    })
    return next.handle(tokenizereq)
      
    }
      
    
  }


