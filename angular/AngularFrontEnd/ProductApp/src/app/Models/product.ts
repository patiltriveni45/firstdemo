export class Product {
    productName?:string;
    productCategory?:string;
    productDescription?:string;
    price?:number;
    image?:string;
}

