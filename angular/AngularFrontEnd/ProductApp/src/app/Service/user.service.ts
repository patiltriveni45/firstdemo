import { Injectable } from '@angular/core';
import { User } from '../Models/user';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Login } from '../Models/login';

@Injectable({
  providedIn: 'root'
})
export class UserService {
 
  

  constructor(private httpClient:HttpClient) { }
  RegisterUser(user: User):Observable<boolean> {
    return this.httpClient.post<boolean>('https://localhost:7058/api/User/RegisterUser',user)
  }
  Login(login: Login):Observable<string> {
    return this.httpClient.post<string>('https://localhost:7058/api/User/Login',login)
  }
}
