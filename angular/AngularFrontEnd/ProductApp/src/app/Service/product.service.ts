import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../Models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  
  private url = "https://localhost:7058/api/Product/GetAllProduct";
  constructor(private httpClient:HttpClient) {}
  GetAllProduct():Observable<Product[]>{
    return this.httpClient.get<Product[]>(this.url)
  }
}
