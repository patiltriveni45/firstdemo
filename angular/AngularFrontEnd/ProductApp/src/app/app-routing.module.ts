import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { LoginComponent } from './Componets/login/login.component';
import { LogoutComponent } from './Componets/logout/logout.component';
import { ProductComponent } from './Componets/product/product.component';
import { RegisterComponent } from './Componets/register/register.component';

const routes: Routes = [
  {path:'register',component:RegisterComponent},
  {path:'login',component:LoginComponent},
  {path:'product',component:ProductComponent,canActivate:[AuthGuard]},
  {path:'logout',component:LogoutComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
