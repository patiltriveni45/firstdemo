import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './Componets/register/register.component';
import {HttpClient, HttpClientModule,HttpInterceptor, HTTP_INTERCEPTORS} from '@angular/common/http';
import { ProductComponent } from './Componets/product/product.component';
import { LoginComponent } from './Componets/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './Componets/nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { HeaderComponent } from './Componets/header/header.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {TokenInterceptorService} from './token-interceptor.service';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { AddProductComponent } from './Componets/Products/add-product/add-product.component';
import { DisplayProductHorizontallyComponent } from './Componets/Products/product/display-product-horizontally/display-product-horizontally.component';
import { DisplayProductVerticallyComponent } from './Componets/Products/product/display-product-vertically/display-product-vertically.component';
import {MatTabsModule} from '@angular/material/tabs';
// import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    AppComponent,
   RegisterComponent,
   ProductComponent,
   LoginComponent,
   NavComponent,
   HeaderComponent,
   AddProductComponent,
   DisplayProductHorizontallyComponent,
   DisplayProductVerticallyComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatButtonModule,
    MatExpansionModule,
    MatFormFieldModule,
    FormsModule,MatInputModule,
    MatTabsModule
    


  ],
  providers: [AuthService ,AuthGuard ,{
    provide:HTTP_INTERCEPTORS,
    useClass:TokenInterceptorService,
    multi:true
  }
    
  ],
  // bootstrap: [ProductComponent]
  bootstrap: [AppComponent]
})
export class AppModule { }
