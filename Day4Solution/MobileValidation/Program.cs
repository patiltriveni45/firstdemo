﻿// See https://aka.ms/new-console-template for more information
using System.Text.RegularExpressions;

public class Validate
{
    public const string pattern = @"^[[A-Z][a-z][0-9][@#]]{8,}$";

    public static bool validatePassword(string password)
    {
        if (password != null) return Regex.IsMatch(password, pattern);
        else return false;
        
    }
}
public class Program
{
    static void Main()
    {
        Console.WriteLine("Enter your password");
        string pass = Console.ReadLine();
        Console.WriteLine(Validate.validatePassword(pass));
    }
}
