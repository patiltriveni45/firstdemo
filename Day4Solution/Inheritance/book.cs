﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    internal class Book : Product
    {
        public Book(string name, double price) : base(name, price)
        {
            this.Price = price;
        }

        public override void AddtoCart(int quantity)
        {
            throw new NotImplementedException();
        }

        public override void Bookdetails()
        {
            throw new NotImplementedException();
        }
    }
}
