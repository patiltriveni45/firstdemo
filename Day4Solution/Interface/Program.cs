﻿// See https://aka.ms/new-console-template for more information
using Interface.Model;
using Interface.Repository;

#region castingreference 
//IOrder order = (IOrder)prepo;
////order.BookOrder();
////order.CancleOrder();
//ICart cart = (ICart)prepo;
//cart.AddToCart();
#endregion


ProductRepo prepo = new ProductRepo();
Product product=new Product() {Name="Mobile",Price=200000};
var cartstatus = prepo.AddToCart(product);
Console.WriteLine(cartstatus);
product = new Product() { Name = "TV", Price = 30000 };
var cartstatus2 = prepo.AddToCart(product);
Console.WriteLine(cartstatus2);

//book order method
List<Product> orderDetails = prepo.BookOrder();
foreach(var order in orderDetails)
{
    Console.WriteLine($" Name:{order.Name}\t Price:{order.Price}");
}
Console.WriteLine("Enter index");
int index = int.Parse(Console.ReadLine());
List<Product> RemoveProduct = prepo.DeleteProduct(index);
foreach(var item in RemoveProduct)
{
    Console.WriteLine($"Name :{item.Name}\t Price {item.Price}");
}

//Cancel order Method
Console.WriteLine(prepo.CancleOrder());
ICart icart=(ICart)prepo;
Console.WriteLine(icart.CancleOrder());