﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.Model
{
    internal class Product

    {

        public String Name { get; set; }
        public Double Price { get; set; }   
    }
}
