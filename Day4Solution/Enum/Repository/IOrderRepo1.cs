﻿using Enum.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enum.Repository
{
    internal interface IOrderRepo1
    {
        void Add(Product product);
        void Update(Product product);
        void Delete(Product product);
    }
}
