﻿using JoinsTask.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7_Tasking
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ShoppingList[] shoppingLists = new ShoppingList[]
            {
                new ShoppingList() { ProductId = 101, Quantity = 1, ShoppingId = 1 },
                new ShoppingList() { ProductId = 102, Quantity = 3, ShoppingId = 2 },
                new ShoppingList() { ProductId = 103, Quantity = 3, ShoppingId = 3 },
                new ShoppingList() { ProductId = 104, Quantity = 1, ShoppingId = 4 },

            };
            Product[] productLists = new Product[]
            {
                new Product(){ProductId=101,ProductName="Nokia"},
                new Product(){ProductId=102,ProductName="Lenovo"},
                new Product(){ProductId=103,ProductName="Acer"},
                new Product(){ProductId=104,ProductName="Dell"},


            };

            Console.WriteLine("Join Operation ");
            var result = from p in productLists
                         join s in shoppingLists
                         on p.ProductId equals s.ProductId
                         select new
                         {
                             pid = p.ProductId,
                             pname = p.ProductName,
                             shoppingquantity = s.Quantity

                         };
            foreach(var item in result)
            {
                Console.WriteLine($"ProductId::{item.pid}\tProductName::{item.pname}\tQuantity::{item.shoppingquantity}");
            }


        }
    }
}
