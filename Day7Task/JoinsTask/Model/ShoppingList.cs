﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoinsTask.Model
{
    internal class ShoppingList
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public int ShoppingId { get; set; }

        
    }
}
