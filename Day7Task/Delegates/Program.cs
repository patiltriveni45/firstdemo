﻿// See https://aka.ms/new-console-template for more information
namespace Delegates
{
    class Program
    {
        

        static void Main()
        {
            Func<int, int> sqr = x => x * x;
            Console.WriteLine(sqr(10));

            Action<int> sqr1 = a => Console.WriteLine($"The square of number {a}::{a*a}");
            sqr1(12);

            Predicate<string>checkbool=a=>a.Count() >4?true:false;
            Console.WriteLine(checkbool("Welcomeeee"));
        }
    }
}
