﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexer
{
    internal class Product
    {
        public int id;
        public string name;
        public int price;
        //public Product(string name,int id)
        //{
        //    this.id= id;
        //    this.name= name;
        //}
        public object this[int index]
        {
            get
            {
                if(index==1)return id;
                else if(index==2)return name;
                else if(index==3)return price;
                return null;
            }
            set
            {
                if (index == 1) id = (int)value;
                else if (index == 2) name = (string)value;
                else if (index==3) price = (int)value;
            }
        }
    }
}
