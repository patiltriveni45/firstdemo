﻿// See https://aka.ms/new-console-template for more information
using Demo.Model;

Product[] product = new Product[]
        {

        new Product(){Id=1,Name="Tv",Price=50000},
        new Product(){Id=1,Name="phone",Price=50000},
        new Product(){Id=1,Name="laptop",Price=50000},
        new Product(){Id=1,Name="phone",Price=50000}
        };
Product1[] product1 = new Product1[]
{
        new Product1(){Name="Tv",Quantity=10},
        new Product1(){Name="laptop",Quantity=5}
};

var result = from p in product
             join p1 in product1
             on p.Name equals p1.Name
             select new
             {
                 pname = p.Name,
                 pQuantity = p1.Quantity,
                 pprice=p.Price*p1.Quantity

             };
foreach (var item in result)
{
    Console.WriteLine($"ProductName::{item.pname}\tQuantity::{item.pQuantity}\tPrice::{item.pprice}");
}
