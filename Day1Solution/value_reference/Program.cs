﻿// See https://aka.ms/new-console-template for more information

#region Value Type
using value_reference;

//Console.WriteLine("Value Type");
//int num1 = 30;
//int num2 = num1;
//Console.WriteLine("before changed");
//Console.WriteLine($"The value of num1 is {num1}");
//Console.WriteLine($"The value of num2 is {num2}");
//num2 = 100;
//Console.WriteLine("After changed");
//Console.WriteLine($"The value of num1 is {num1}");
//Console.WriteLine($"The value of num2 is {num2}");
#endregion

Console.WriteLine("reference Type");
Sample obj1 = new Sample();
Sample obj2 = obj1;
Console.WriteLine(obj1.width);
Console.WriteLine(obj2.width);

Console.WriteLine("after change");
obj1.width = 30;
Console.WriteLine(obj1.width);
Console.WriteLine(obj2.width);