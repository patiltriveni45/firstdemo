﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Model
{
    internal class Product
    {
        internal int ProductID;
        internal string? ProductName;
        internal int ProductPrice;

        public Product(int PID, string PName, int P_Price)
        {
            this.ProductID= PID;
            this.ProductName   = PName;
            this.ProductPrice = P_Price;
        }

    }
}
