﻿// See https://aka.ms/new-console-template for more information

using ClassLibraryDotnet2._0;
using ClassLibraryDotnet6;
using ConsoleAppDotnet6;
using ConsoleAppDotnet6.repository;
#region varible
//explicitly 
//int age = 30;
//Console.WriteLine(age);
//int userAge = 60;
//Console.WriteLine($"user age is :{userAge}");
//implicitly

//var age = 30;
//Console.WriteLine(age.GetType());
//Console.WriteLine(typeof(int));
//Console.WriteLine(age);

//implicitly type conversion
int age2 = 60;
double UserAge=age2;
Console.WriteLine(UserAge.GetType());

//explicitly type conversion 
double marks = 33.45;
int marks3 = (int)marks;
Console.WriteLine(marks3.GetType());

#endregion

#region user application
//user application
//username,userage,user password
//Console.WriteLine("enter user name :");
//string username = Console.ReadLine();
//Console.WriteLine("enter user Age :");
//int userAge = int.Parse(Console.ReadLine());
//Console.WriteLine("enter user password :");
//int userPassword = int.Parse(Console.ReadLine());
//Console.WriteLine($"user name is {username}\t user age is{userAge}\t user password is{userPassword}");
#endregion

#region  product application
//Console.WriteLine("enter product name :");
//string product_name=Console.ReadLine();
//Console.WriteLine("enter product price : ");
//int product_price=int.Parse(Console.ReadLine());
//Console.WriteLine("enter product review :");
//int product_review=int.Parse(Console.ReadLine());
//Console.WriteLine("product quality : ");
//string product_quality=Console.ReadLine();
//Console.WriteLine($"product name is {product_name}\n product_price is {product_price}\n product review is{product_review}\n product quality is {product_quality}");

#endregion

#region stadard libraray
//Class1 obj1 = new Class1();
//obj1.firstname = "Triveni";
//obj1.lastname = "Patil";
//Console.WriteLine(obj1.GetFullName());

//User obj2 = new User();
//obj2.name = "Trivei";
//Console.WriteLine(obj2.GetName());
#endregion

#region ternary operator
//string username = "trivenipatil";
//int password = 122;
//Console.WriteLine("enter username : ");
//string enterusername = Console.ReadLine();
//Console.WriteLine("enter password :");
//int enterpassword = int.Parse(Console.ReadLine());
//if (username == enterusername || password == enterpassword)
//{
//    Console.WriteLine("login successfully ");
//}
//else
//{
//    Console.WriteLine("usernam and password is not same ");
//}

//int a = default;
//string name = default;
//char lname = default;
//bool ab = default;
//Console.WriteLine(ab);
//Console.WriteLine(lname);
#endregion



#region create struct objec
//Console.WriteLine("value type");
Customer cus_obj = new Customer();
Product product_detais = new Product("iphone", "20000", "iphone quality is good");
cus_obj.id = 101;
cus_obj.cus_name = "triveni";
cus_obj.cus_product = product_detais;
Console.WriteLine($"id :{cus_obj.id}\t name : {cus_obj.cus_name}\t cus_product : {cus_obj.cus_product.product_name}\t {cus_obj.cus_product.product_pric e}\t{cus_obj.cus_product.product_description}");
#endregion


//Console.WriteLine("login functionality");
//Console.WriteLine("username");
//Console.WriteLine("password");
//UserRepository userrepos = new UserRepository();
//Console.WriteLine(userrepos.login(username,password);
