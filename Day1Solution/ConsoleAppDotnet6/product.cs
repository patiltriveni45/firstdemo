﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppDotnet6
{
    struct Product
    {
        public string product_name;
        public string product_description;
        public string product_price;

        public Product(string pname,string pdesc,string pprice)
        {
            this.product_name = pname;
            this.product_description = pdesc;
            this.product_price = pprice;
        }
    }
}
