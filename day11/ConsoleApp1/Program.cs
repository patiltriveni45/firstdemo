﻿using ConsoleApp1.Model;
using ConsoleApp1.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ProductRepository repo = new ProductRepository();
            List<Product> products = repo.GetAllProduct();
            foreach(Product item in products)
            {
                Console.WriteLine(item);
            }
        }
    }
}
