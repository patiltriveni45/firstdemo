namespace TestProject1
{
    public class XUnitTest1
    {
        [Fact]
        public void Test1()
        {
            //initialization or arrange
            var temp1 = 32.9;
            var expected = 91.22;
            var actual = TempConverter.celciustofarenheit(temp1);
            Assert.Equal(expected, actual);
        }
       
    }
}