﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqDemo.Model
{
    internal class Product
    {
      public string Name { get; set; }
      public string Category { get; set; }
      public string Make { get; set; }
      public int Price { get; set; }

        public override string ToString()
        {
            return $"Name:;{Name}\tCatrogory:;{Category}\tMake::{Make}\tPrice::{Price};
        }
    }
   
}
