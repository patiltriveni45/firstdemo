﻿// See https://aka.ms/new-console-template for more information
namespace MultiCast;

class Program
{
    delegate void Mailer(string email);
    public static void Main()
    {
        Console.WriteLine("Delegate Demo");
        Mailer mailer = Externalmail;
        mailer += Internalmail;
        mailer("Multicast Delegate");

    }
    public static void Internalmail(string message)
    {
        Console.WriteLine($"internal Mail::{message}");
    }
    public static void Externalmail(string message)
    {
        Console.WriteLine($"External Mail::{message}");
    }
}