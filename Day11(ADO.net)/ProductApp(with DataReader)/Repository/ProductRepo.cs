﻿using ProductApp_with_DataReader_.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp_with_DataReader_.Repository
{
    internal class ProductRepo
    {
        SqlConnection con = null;
        SqlCommand cmd = null;
        public ProductRepo()
        {
            string cs = "Data Source=DESKTOP-TGO6IK8;Database=Productdb;integrated Security=true";
            con = new SqlConnection(cs);
            cmd = new SqlCommand();
        }
        public int AddProduct()
        {
            Console.WriteLine("Enter Product Id");
            string Id = Console.ReadLine();
            Console.WriteLine("Enter Product Name");
            string Productname = Console.ReadLine();
            Console.WriteLine("Enter Product Price");
            string Price = Console.ReadLine();
            Console.WriteLine("Enter  Category Id");
            string CatId = Console.ReadLine();
            Console.WriteLine("Enter Rating");
            string Rating = Console.ReadLine();
            cmd.CommandText = "Insert into ProductTbl values(@id,@name,@price,@Catid,@rating)";
            cmd.Parameters.AddWithValue("@id", Id);
            cmd.Parameters.AddWithValue("@name", Productname);
            cmd.Parameters.AddWithValue("@price", Price);
            cmd.Parameters.AddWithValue("@Catid", CatId);
            cmd.Parameters.AddWithValue("@rating", Rating);
            cmd.Connection = con;
            con.Open();
            int AddProductStatus = cmd.ExecuteNonQuery();
            con.Close();
            return AddProductStatus;
        }
        public List<Product> GetAllProduct()
        {
            List<Product> products = new List<Product>();
            string query = "select * from ProductTbl";
            cmd.CommandText = query;
            cmd.Connection = con;
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Product product = new Product();
                product.Id = (int)reader["Id"];
                product.ProductName = (string)reader["ProductName"];
                product.Price = (int)reader["Price"];
                product.CatId = (int)reader["CatId"];
                product.Rating = (double)reader["Rating"];

                products.Add(product);
            }
            con.Close();
            return products;
            Console.ReadLine();
        }
        public int UpdateProduct()
        {
            Console.WriteLine("Enter Id that You want to update");
            int Id = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter New Product Name");
            string productname = Console.ReadLine();
            cmd.CommandText = "update ProductTbl set productname=@name1 where Id=@id1";
            cmd.Parameters.AddWithValue("@id1", Id);
            cmd.Parameters.AddWithValue("@name1", productname);
            cmd.Connection = con;
            con.Open();
            int UpdateProductStatus = cmd.ExecuteNonQuery();
            con.Close();
            return UpdateProductStatus;
        }
        public int DeleteProduct()
        {
            Console.WriteLine("Enter Id that You want to Delete");
            int Id = int.Parse(Console.ReadLine());
            cmd.CommandText = "Delete from ProductTbl where Id=@id2 ";
            cmd.Parameters.AddWithValue("@id2", Id);
            cmd.Connection = con;
            con.Open();
            int DeleteProductStatus = cmd.ExecuteNonQuery();
            con.Close();
            return DeleteProductStatus;
        }
    }
}
