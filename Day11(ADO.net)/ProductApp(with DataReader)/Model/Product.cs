﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp_with_DataReader_.Model
{
    internal class Product
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public int Price { get; set; }
        public int CatId { get; set; }
        public double Rating { get; set; }

        public override string ToString()
        {
            return $"Id::{Id}\t ProductName::{ProductName}\t Price::{Price}\t CatId::{CatId}\t Rating::{Rating}";
        }
    }
}
