﻿// See https://aka.ms/new-console-template for more information


using ProductApp_with_DataReader_.Model;
using ProductApp_with_DataReader_.Repository;

ProductRepo repo = new ProductRepo();


bool iscontinue = true;
while (iscontinue)
{
    Console.WriteLine($"0.Exit \t1.Add product\t2.Update Product\t3.Delete Product\t4.Display Product");
    int choice = int.Parse(Console.ReadLine());
    switch (choice)
    {

        case 0:
            iscontinue = false;
            break;
        case 1:
            repo.AddProduct();
            
            break;
        case 2:
            repo.UpdateProduct();
            break;
        case 3:
            repo.DeleteProduct();
            break;
        case 4:

            List<Product> products = repo.GetAllProduct();
            foreach (Product item in products)
            {
                Console.WriteLine(item);
            }
            break;
        

        default:
            Console.WriteLine("Invalid input");
            break;
    }
}