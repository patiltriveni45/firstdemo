﻿using ProductApp_With_Adapter_.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp_With_Adapter_.Repository
{
    internal class ProductDataTable
    {
        SqlDataAdapter sqldataAdapter = null;
        DataTable datatable = null;
        DataSet dataset = null;
        public ProductDataTable()
        {
            sqldataAdapter = new SqlDataAdapter("select * from StudTbl", "Data Source=DESKTOP-TGO6IK8;Database=StudentDB;integrated Security=true");
            datatable = new DataTable();
            dataset = new DataSet();
            sqldataAdapter.Fill(datatable);
            dataset.Tables.Add(datatable);
        }
        public List<Product> GetAllProduct()
        {
            List<Product> products = new List<Product>();
            foreach (DataRow row in dataset.Tables[0].Rows)
            {
                Product product = new Product();
                product.StudId = (int)row["StudId"];
                product.StudName = (string)row["StudName"];
                product.Age = (int)row["Age"];
                products.Add(product);

            }
            return products;
        }
    }
}
