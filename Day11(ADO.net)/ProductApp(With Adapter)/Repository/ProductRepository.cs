﻿using ProductApp_With_Adapter_.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp_With_Adapter_.Repository
{
    internal class ProductRepository
    {
        SqlDataAdapter sqldataAdapter = null;
        DataSet dataset = null;

        public ProductRepository()
        {            
            sqldataAdapter = new SqlDataAdapter("select * from StudTbl", "Data Source=DESKTOP-TGO6IK8;Database=StudentDB;integrated Security=true");
            dataset = new DataSet();
            sqldataAdapter.Fill(dataset);
        }
        public List<Product> GetAllProduct()
        {
            List<Product> products = new List<Product>();
            foreach(DataRow row in dataset.Tables["t_student"].Rows)
            {
                Product product = new Product();
                product.StudId = (int)row["StudId"];
                product.StudName = (string)row["StudName"];
                product.Age = (int)row["Age"];
                products.Add(product);  

            }
            return products;
        }

        public void AddProduct()
        {

        }

    }
}
