﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp_With_Adapter_.Model
{
    internal class Product
    {
        public int StudId { get; set; }
        public string StudName { get; set; }
        public int Age { get; set; }

        public override string ToString()
        {
            return $"StudentId::{StudId}\t Studentname::{StudName}\tAge::{Age}";
        }
    }
}
