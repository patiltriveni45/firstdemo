﻿using Microsoft.EntityFrameworkCore;
using MVC_Project.Models;

namespace MVC_Project.Context
{
    public class DBContext: DbContext
    {
        public DBContext(DbContextOptions<DBContext> Context) : base(Context)
        {

        }
        public DbSet<Product> ProductTbl{ get; set; }
        public DbSet<UserInfo> UserTbl{ get; set; }

    }
}
