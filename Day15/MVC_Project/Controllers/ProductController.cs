﻿using Microsoft.AspNetCore.Mvc;
using MVC_Project.Service;

namespace MVC_Project.Controllers
{
    public class ProductController : Controller
    {
        readonly IProductService _productservice;
        public ProductController(IProductService productservice)
        {
            _productservice = productservice;
        }
        public IActionResult GetAllProduct()
        {
            _productservice.GetAllProduct();
            return View();
        }
    }
}
