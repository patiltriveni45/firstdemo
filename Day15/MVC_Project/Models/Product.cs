﻿using System.ComponentModel.DataAnnotations;

namespace MVC_Project.Models
{
    public class Product
    {
        [Key]
        public int ProductID { get; set; }
        [Required]
        public string ProductName { get; set; }
        
        public  string ProductCategory { get; set; }

        public  double Price{ get; set; }
    }
}
