﻿using System.ComponentModel.DataAnnotations;

namespace ProductMvcProject.Models
{
    public class LoginInfo
    {
        [Required]
        
        public string Username { get; set; }

        [Required]
       
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
