﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductMvcProject.Models
{
    public class ShoppingCart
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int CartID{ get; set; }
        public int UserID { get; set; }
        public int ProductID { get; set; }
        [Required]
        [StringLength(20)]
        public string ProductName { get; set; }

        [Required]
        [StringLength(20)]
        public string? ProductCategory { get; set; }

        [Required]
        [Column(TypeName = "money")]
        public double Price { get; set; }

        [DefaultValue(1)]
        public int Quantity { get; set; }

        public string? ImageUrl { get; set; }

        public char Isdeleted { get; set; } = 'A';

    }
}
