﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductMvcProject.Models
{
    public class Order
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int OrderId { get; set; }
        public string Invoice { get; set; }
        public int UserID { get; set; }
        public int ProductID { get; set; }

        public string ProductName { get; set; }

        public double Price { get; set; }

        public int Quantity { get; set; }

        public string Status { get; set; }

        public DateTime OrderDate { get; set; } = DateTime.Now;



    }
}
