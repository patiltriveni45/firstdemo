﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using RemoteAttribute = Microsoft.AspNetCore.Mvc.RemoteAttribute;

namespace ProductMvcProject.Models
{
    public class UserInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int UserID { get; set; }

        [Required]
        [Remote("IsUsernameExist", "User", HttpMethod = "POST", ErrorMessage = "Username Already Exist")]
        [RegularExpression(@"^[A-Za-z]{4,29}$", ErrorMessage ="Please Enter valid Username")]
        
        public string? UserName { get; set; }


        [Required]
        [RegularExpression(@"^[0-9a-zA-z]+[.+-_$]{0,1}[0-9a-zA-z]+[@][a-zA-z]+[.][a-zA-z]{2,3}$", ErrorMessage = "Please Enter Valid Email")]
        public string? Email { get; set; }

        [Required]
        [RegularExpression(@"^[a-zA-z0-9@&*$]{8,50}", ErrorMessage = "Password Should  include Capital Letter ,Small letter and special Symbol")]
        [DataType(DataType.Password)]
        public string? Password { get; set; }


        [Required]
        [RegularExpression(@"^[6-9]{1}[0-9]{9}", ErrorMessage = "Please Enter valid Mobile Number")]
        public string? PhoneNumber { get; set; }

        [Required]
        [RegularExpression(@"^[A-Za-z]{4,20}", ErrorMessage = "Please Enter valid Address ")]
        public string? Address { get; set; }
       
        public DateTime Created_At { get; set; } = DateTime.Now;

        [DefaultValue(false)]
        public bool IsAdmin { get; set; }
    }
}
