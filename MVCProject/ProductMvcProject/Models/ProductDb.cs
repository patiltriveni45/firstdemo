﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductMvcProject.Models
{
    public class ProductDb
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]  
        public int ProductID { get; set; }

       
        [Required]
        [StringLength(20)]
        [Column("Name")]
        public string ProductName { get; set; }

        [Required]
        [StringLength(20)]
        [Column("Category")]
        public string? ProductCategory { get; set; }

        [Required]
        [StringLength(200)]
        [Column("Description")]
        public string? ProductDescription { get; set; }


        [Required]
        [Column(TypeName="money")]
        
        public double Price { get; set; }


        [Column("Modified Date")]

        public DateTime Modified_at{ get; set; } = DateTime.Now;

        [Column("Image")]
        public string? ImageUrl { get; set; }

        [NotMapped]
        public IFormFile?  Image { get;set; }

    }
}
