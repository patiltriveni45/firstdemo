﻿using ProductMvcProject.Models;

namespace ProductMvcProject.Service
{
    public interface IProductService
    {
        List<ProductDb> ViewProduct();
        void AddToCart(int id, int userID);
        List<ShoppingCart> Viewcart(int userID);
       bool DeleteFromCart(int id);
        void DeleteFromCart(List<ShoppingCart> cart);
        bool AddOrder(List<Order> order);
        List<Order> ViewOrder(int userID);
    }
}
