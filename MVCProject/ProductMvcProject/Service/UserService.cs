﻿using ProductMvcProject.Exception;
using ProductMvcProject.Models;
using ProductMvcProject.Repository;

namespace ProductMvcProject.Service
{
    public class UserService:IUserService
    {
        readonly IUserRepository _userrepository;
        public UserService(IUserRepository userRepository)
        {
            _userrepository= userRepository;
        }

        public UserInfo GetUserByName(string username)
        {
            return _userrepository.GetUserByName(username);
        }

        public UserInfo Login(LoginInfo loginInfo)
        {
            UserInfo user = _userrepository.GetUserByLogin(loginInfo.Username, loginInfo.Password);
            if (user != null)
            {
                return user;
            }
            else
            {
                throw new UserCreditionalInvalidException($"Username  is Not Register");
            }
        }

        

        public void RegisterUser(UserInfo userinfo)
        {
            _userrepository.Registeruser(userinfo);
        }
    }
}
