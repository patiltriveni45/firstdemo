﻿using ProductMvcProject.Exception;
using ProductMvcProject.Models;
using ProductMvcProject.Repository;

namespace ProductMvcProject.Service
{
    public class ProductService:IProductService
    {
        readonly IProductRepositroy _productRepository;
        public ProductService(IProductRepositroy productRepositroy)
        {
            _productRepository =productRepositroy;
        }

        public void AddToCart(int id, int userID)
        {
            ShoppingCart cart = _productRepository.GetCartById(id);
            //var userId = _productRepository.GetUserById(userID);
            
            if (cart==null)
            {
                _productRepository.AddToCart(id, userID);   
            }
            else
            {
                //_productRepository.UpdateQuantity(id);
                //throw new CartAlreadyExists($"Item Already Exists in cart");
            }
        }

        public bool DeleteFromCart(int id)
        {
            var cart_Exist = _productRepository.GetCartById(id);
            if (cart_Exist != null)
            {
                return _productRepository.DeleteFromCart(id);
            }
            else return false;
        }

        

        public List<ShoppingCart> Viewcart(int userID)
        {
            return _productRepository.ViewCart(userID);
        }

        public List<ProductDb> ViewProduct()
        {
            return _productRepository.ViewProduct();
        }


        public void DeleteFromCart(List<ShoppingCart> cart)
        {
            _productRepository.DeleteFromCart(cart);
        }
        public bool AddOrder(List<Order> order)
        {
            return _productRepository.AddOrder(order);
        }

        public List<Order> ViewOrder(int id)
        {
            return _productRepository.ViewOrder(id);
;
        }
    }
 }

