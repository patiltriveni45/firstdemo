﻿using ProductMvcProject.Models;

namespace ProductMvcProject.Service
{
    public interface IUserService
    {
        void RegisterUser(UserInfo userinfo);
        UserInfo GetUserByName(string username);
        
        UserInfo Login(LoginInfo loginInfo);
    }
}
