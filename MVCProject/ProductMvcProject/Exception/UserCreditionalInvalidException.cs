﻿namespace ProductMvcProject.Exception
{
    public class UserCreditionalInvalidException : ApplicationException
    {
        public UserCreditionalInvalidException(string? message) : base(message)
        {
        }
    }
}
