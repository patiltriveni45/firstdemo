﻿namespace ProductMvcProject.Exception
{
    public class CartIsEmpty : ApplicationException
    {
        public CartIsEmpty(string? message) : base(message)
        {
        }
    }
}
