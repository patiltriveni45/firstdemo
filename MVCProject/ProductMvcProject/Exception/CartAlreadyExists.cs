﻿namespace ProductMvcProject.Exception
{
    public class CartAlreadyExists : ApplicationException
    {
        public CartAlreadyExists(string? message) : base(message)
        {
        }
    }
}
