﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProductMvcProject.Context;
using ProductMvcProject.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;
namespace ProductMvcProject.Controllers
{
    public class AdminDashboardController : Controller
    {
        readonly Product_DbContext _product_DbContext;
        readonly IWebHostEnvironment webHostEnvironment;
        public AdminDashboardController(Product_DbContext product_DbContext, IWebHostEnvironment webHost)
        {
            _product_DbContext = product_DbContext;
            webHostEnvironment = webHost;
        }

        [Authorize]
        public IActionResult Index()
        {
            List<ProductDb> products = _product_DbContext.ProductTbl.ToList();
            return View(products);
        }
        [HttpGet]
        public ActionResult Create()
        {
            ProductDb productDb = new ProductDb();
            return View(productDb);
        }
        [HttpPost]
        public ActionResult Create(ProductDb product)
        {
            if (ModelState.IsValid)
            {
                string uniquefilename = Uploadfile(product);
                product.ImageUrl = uniquefilename;
                _product_DbContext.Attach(product);
                _product_DbContext.Entry(product).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                _product_DbContext.SaveChanges();
                return RedirectToAction(nameof(Index));
            }

            return View(product);
        }
        private string Uploadfile(ProductDb product)
        {
            string? uniquefilename = null;
            if (product.Image != null)
            {
                string uploadsfolder = Path.Combine(webHostEnvironment.WebRootPath, "Images");
                uniquefilename = Guid.NewGuid().ToString() + "_" + product.Image.FileName;
                string filepath = Path.Combine(uploadsfolder, uniquefilename);
                using (var filestream = new FileStream(filepath, FileMode.Create))
                {
                    product.Image.CopyTo(filestream);
                }


            }
            return uniquefilename;
        }
        [HttpGet]
        public ActionResult EditProduct(int id)
        {
            var product = _product_DbContext.ProductTbl.Where(u => u.ProductID == id).FirstOrDefault();
            return View(product);
        }
        [HttpPost]
        public ActionResult EditProduct(ProductDb product, int id, IFormFile? file)
        {

            if (id == null)
            {
                return NotFound();
            }

            ProductDb product1 = _product_DbContext.ProductTbl.Where(u => u.ProductID == id).FirstOrDefault();
            if (product1 == null)
            {
                return NotFound();
            }
            if (file != null || file?.Length != 0)
            {
                String filename = System.Guid.NewGuid().ToString() + ".jpg";
                var path = Path.Combine(webHostEnvironment.WebRootPath, "Images", filename);
                using (var filestream = new FileStream(path, FileMode.Create))
                {
                    file?.CopyTo(filestream);
                }
                product1.ImageUrl = filename;
            }
            product1.ProductName = product.ProductName;
            product1.ProductCategory = product.ProductCategory;
            product1.ProductDescription = product.ProductDescription;
            product1.Price = product.Price;
            product1.Modified_at = product.Modified_at;

            _product_DbContext.SaveChanges();
            return RedirectToAction("Index", "AdminDashboard");

            //if (ModelState.IsValid)
            //{
            //    _product_DbContext.Entry<ProductDb>(product).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            //    _product_DbContext.SaveChanges();
            //    return RedirectToAction("Index", "AdminDashboard");

            //}

            //return View(product);
        }
        [HttpGet]
        public ActionResult DetailsProduct(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProductDb product = _product_DbContext.ProductTbl.Where(u => u.ProductID == id).FirstOrDefault();
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }
        [HttpGet]
        
        public ActionResult DeleteProduct(int id)
        {
            var productexist = _product_DbContext.ProductTbl.Where(u => u.ProductID == id).FirstOrDefault();
            return View(productexist);
        }
        [HttpPost]
        public ActionResult DeleteProduct(int id,ProductDb product1)
        {
            ProductDb product = GetProductById(id);
            _product_DbContext.ProductTbl.Remove(product);
            _product_DbContext.SaveChanges();
            return RedirectToAction("Index");
           
        }
        public ProductDb GetProductById(int id)
        {
            return _product_DbContext.ProductTbl.Where(u => u.ProductID == id).FirstOrDefault();
        }


    }

} 

