﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductMvcProject.Context;
using ProductMvcProject.Exception;
using ProductMvcProject.Models;
using ProductMvcProject.Service;
using System.Text.Json;

namespace ProductMvcProject.Controllers
{
    public class UserDashboardController : Controller

    {
        Product_DbContext _productdbcontext;
        readonly IProductService _productservice;
        //readonly Product_DbContext _product_Db;
        public UserDashboardController(IProductService productService, Product_DbContext product_DbContext)
        {
           _productservice = productService;
            _productdbcontext = product_DbContext;  
        }
        [Authorize]
        //public IActionResult Index()

        //{

        //    return View();
        //}
        [Authorize]
        public ActionResult ViewProduct()


        {
            IEnumerable<ProductDb> products = _productservice.ViewProduct();
            return View(products);
            

        }
        public ActionResult AddTocart(int id,ProductDb product)
        {
            try
            {

                int UserID = (int)HttpContext.Session.GetInt32("UseId");
                _productservice.AddToCart(id, UserID);
                TempData["success"] = "Added Succefully To the cart";
                return RedirectToAction("ViewProduct");
            }
            catch(CartAlreadyExists caie)
            {
                TempData["error"] = caie.Message;
                return RedirectToAction("ViewProduct");
            }
           
                
            
            
        }

        public ActionResult Viewcart()
        {
            int UserID = (int)HttpContext.Session.GetInt32("UseId");
            List<ShoppingCart> cart = _productservice.Viewcart(UserID);
            return View(cart);
        }
        public ActionResult DeleteFromCart(int id)
        {
            _productservice.DeleteFromCart(id);
            return RedirectToAction("Viewcart");
        }
        public ActionResult CheckOut()
        {
            int UserID = (int)HttpContext.Session.GetInt32("UseId");
            List<ShoppingCart> cart = _productservice.Viewcart(UserID);
            string InvoiceNo = Guid.NewGuid().ToString();
            List<Order> order = new List<Order>();
            cart.ForEach(item =>
            {
                order.Add(new Order()
                {
                    ProductID = item.ProductID,
                    ProductName = item.ProductName,
                    Invoice = InvoiceNo,
                    UserID=item.UserID,
                    Quantity=item.Quantity,
                    Price=item.Price*item.Quantity,
                    Status="Ordered"



                });
            });
            int total = 0;
            foreach(Order item in order)
            {
                total +=(int)item.Price;
                
            }
            TempData["total"] =total;
            bool res = _productservice.AddOrder(order);
            if (res == true)
            {
                _productservice.DeleteFromCart(cart);

            }
            return RedirectToAction("ViewOrder");
        }
        public IActionResult ViewOrder()
        {
            int UserID = (int)HttpContext.Session.GetInt32("UseId");
            var  order = _productservice.ViewOrder(UserID).DistinctBy(item => item.Invoice).ToList();

            return View(order);
        }

        public ActionResult Addcartitem(int id)
        {
           
            ShoppingCart cart=_productdbcontext.CartTbl.FirstOrDefault(u=>u.CartID== id);
            cart.Quantity += 1;
            _productdbcontext.Entry<ShoppingCart>(cart).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _productdbcontext.SaveChanges();
            return RedirectToAction("Viewcart");

        }
        public ActionResult Subcartitem(int id)
        {
            ShoppingCart cart = _productdbcontext.CartTbl.FirstOrDefault(u => u.CartID == id);
            if (cart.Quantity > 1)
            {
                cart.Quantity -= 1;
                _productdbcontext.Entry<ShoppingCart>(cart).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _productdbcontext.SaveChanges();
                return RedirectToAction("Viewcart");
            }
            else
            {
                return RedirectToAction("Viewcart");
            }
            

        }
        [HttpGet]
        public IActionResult ViewOrderDetails(string id)
        {
            int UserID = (int)HttpContext.Session.GetInt32("UseId");
           
            if (UserID != null)
            {
                //ViewBag.User = user;
            }
            else
            {
                return RedirectToRoute(new { controller = "User", action = "Login" });
            }
            var olist =_productservice.ViewOrder(UserID).Where(item => item.Invoice == id).ToList();
            
            ViewBag.Invoice = id;
            double orderTotal = 0;
            olist.ForEach(o =>
            {
                orderTotal += o.Quantity * o.Price;
            });
            ViewBag.OrderTotal = orderTotal;
            return View(olist);
        }



    }
}


