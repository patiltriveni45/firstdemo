﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductMvcProject.Exception;
using ProductMvcProject.Models;
using ProductMvcProject.Service;
using System.Security.Claims;

namespace ProductMvcProject.Controllers
{
    public class UserController : Controller
    {
        readonly IUserService _userservice;
        
        public UserController(IUserService userservice)
        {
            _userservice = userservice;
        }
       
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(UserInfo userinfo)
        {
            if (ModelState.IsValid)
            {
                _userservice.RegisterUser(userinfo);
                
                return RedirectToAction("Login");
                

            }
            return View(userinfo);
                
           
        }
        [HttpPost]
        public JsonResult IsUsernameExist(string UserName)
        {
            var user = _userservice.GetUserByName(UserName);
            return Json(user==null);
        }
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginInfo loginInfo)
        {
            try 
            {
                UserInfo user = _userservice.Login(loginInfo);
                var identity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, loginInfo.Username) },
                CookieAuthenticationDefaults.AuthenticationScheme);
                var principle=new ClaimsPrincipal(identity);
                HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principle);
                HttpContext.Session.SetString("Username", loginInfo.Username);
                HttpContext.Session.SetInt32("UseId", user.UserID);
                if (user.IsAdmin)
                {
                    return RedirectToAction("Index", "AdminDashboard");
                }
                else
                {
                    return RedirectToAction("ViewProduct", "UserDashboard");
                }
                

            }
            catch(UserCreditionalInvalidException ucie)
            {
                TempData["alert"] = ucie.Message;
                return RedirectToAction("Login");
            }
            
        }

        public ActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            var storedCookies = Request.Cookies.Keys;
            foreach(var Cookies in storedCookies)
            {
                Response.Cookies.Delete(Cookies);
            }
            return RedirectToAction("Login", "User");
        }
        public ActionResult Dashboard()
        {
            return View();
        }


    }
}
