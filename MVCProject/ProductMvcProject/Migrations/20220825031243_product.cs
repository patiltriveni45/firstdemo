﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProductMvcProject.Migrations
{
    /// <inheritdoc />
    public partial class product : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "ProductTbl");

            migrationBuilder.AlterColumn<string>(
                name: "ImageUrl",
                table: "ProductTbl",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddColumn<string>(
                name: "ProductDescription",
                table: "ProductTbl",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProductDescription",
                table: "ProductTbl");

            migrationBuilder.AlterColumn<string>(
                name: "ImageUrl",
                table: "ProductTbl",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "ProductTbl",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
