﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProductMvcProject.Migrations
{
    /// <inheritdoc />
    public partial class te : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AdminId",
                table: "AdminLoginTbl",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AdminLoginTbl",
                table: "AdminLoginTbl",
                column: "AdminId");

            migrationBuilder.CreateTable(
                name: "LoginInfo",
                columns: table => new
                {
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LoginInfo");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AdminLoginTbl",
                table: "AdminLoginTbl");

            migrationBuilder.DropColumn(
                name: "AdminId",
                table: "AdminLoginTbl");
        }
    }
}
