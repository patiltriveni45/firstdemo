﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProductMvcProject.Migrations
{
    /// <inheritdoc />
    public partial class tr : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "LoginTbl",
                newName: "AdminLoginTbl");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "AdminLoginTbl",
                newName: "LoginTbl");
        }
    }
}
