﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProductMvcProject.Migrations
{
    /// <inheritdoc />
    public partial class change : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProductName",
                table: "ProductTbl",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "ProductDescription",
                table: "ProductTbl",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "ProductCategory",
                table: "ProductTbl",
                newName: "Category");

            migrationBuilder.RenameColumn(
                name: "Modified_at",
                table: "ProductTbl",
                newName: "Modified Date");

            migrationBuilder.RenameColumn(
                name: "ImageUrl",
                table: "ProductTbl",
                newName: "Image");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "ProductTbl",
                newName: "ProductName");

            migrationBuilder.RenameColumn(
                name: "Modified Date",
                table: "ProductTbl",
                newName: "Modified_at");

            migrationBuilder.RenameColumn(
                name: "Image",
                table: "ProductTbl",
                newName: "ImageUrl");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "ProductTbl",
                newName: "ProductDescription");

            migrationBuilder.RenameColumn(
                name: "Category",
                table: "ProductTbl",
                newName: "ProductCategory");
        }
    }
}
