﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProductMvcProject.Migrations
{
    /// <inheritdoc />
    public partial class isdeletd : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdminLoginTbl");

            migrationBuilder.AddColumn<string>(
                name: "Isdeleted",
                table: "CartTbl",
                type: "nvarchar(1)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Isdeleted",
                table: "CartTbl");

            migrationBuilder.CreateTable(
                name: "AdminLoginTbl",
                columns: table => new
                {
                    AdminId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdminLoginTbl", x => x.AdminId);
                });
        }
    }
}
