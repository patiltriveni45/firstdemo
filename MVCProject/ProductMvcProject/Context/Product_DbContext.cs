﻿using Microsoft.EntityFrameworkCore;
using ProductMvcProject.Models;

namespace ProductMvcProject.Context
{
    public class Product_DbContext:DbContext
    {
        public Product_DbContext(DbContextOptions<Product_DbContext>Context):base(Context)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LoginInfo>().HasNoKey();
            
        }

       



        public DbSet<ProductDb> ProductTbl { get; set; }
        public DbSet<UserInfo> UserTbl { get; set; }
       
        public DbSet<Order> OrderTbl { get; set; }

        public DbSet<ShoppingCart> CartTbl { get; set; }

       

    }
}
