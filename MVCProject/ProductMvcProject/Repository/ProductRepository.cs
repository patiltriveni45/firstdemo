﻿using Microsoft.EntityFrameworkCore;
using ProductMvcProject.Context;
using ProductMvcProject.Models;
using System.Linq;

namespace ProductMvcProject.Repository
{
    public class ProductRepository:IProductRepositroy
    {
        Product_DbContext _productdbcontext;
        public ProductRepository(Product_DbContext product_DbContext)
        {
            _productdbcontext=product_DbContext;
        }

        public void AddToCart(int id, int userID)
        {
            ShoppingCart cart=new ShoppingCart();
            ProductDb product = GetProductById(id);
            if(product!=null)
            {
                cart.ImageUrl = product.ImageUrl;
                cart.ProductID = product.ProductID;
                cart.ProductName= product.ProductName;
                cart.ProductCategory = product.ProductCategory;
                cart.Price = product.Price;
                cart.UserID=userID;
                cart.Quantity = 1;
                _productdbcontext.CartTbl.Add(cart);
                _productdbcontext.SaveChanges();


            }

        }

        public ShoppingCart GetCartById(int id)
        {
            return _productdbcontext.CartTbl.Where(u => u.ProductID == id).FirstOrDefault();
        }

        public void UpdateQuantity(int id)
        {
            ShoppingCart cart=GetCartById(id);

            cart.Quantity += 1;
            cart.Price *= cart.Quantity;
            _productdbcontext.Entry<ShoppingCart>(cart).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
           _productdbcontext.SaveChanges();
               
        }

        public List<ProductDb> ViewProduct()
        {
            return _productdbcontext.ProductTbl.ToList();
        }
        public ProductDb GetProductById(int id)
        {
            return _productdbcontext.ProductTbl.Where(u => u.ProductID == id).FirstOrDefault();
        }

        public List<ShoppingCart> ViewCart(int userID)
        {
            return _productdbcontext.CartTbl.Where(u => u.UserID == userID).ToList();
        }

        public bool DeleteFromCart(int id)
        {
            ShoppingCart cart = GetCartById(id);
            _productdbcontext.CartTbl.Remove(cart);
            return _productdbcontext.SaveChanges() == 1 ? true : false;
            return true;
            
        }
        public void DeleteFromCart(List<ShoppingCart> cart)
        {
            foreach (ShoppingCart item in cart)
            {
                _productdbcontext.Entry(item).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _productdbcontext.SaveChanges();
            }

        }

        public bool AddOrder(List<Order> order)

        {
            foreach (Order item in order)
            {
                _productdbcontext.OrderTbl.Add(item);
                
            }

            return _productdbcontext.SaveChanges() == 1 ? true : false;
        }

        public List<Order> ViewOrder(int id)
        {
            //return _productdbcontext.OrderTbl.Where(u => u.UserID == id).ToList();
            var orderItems = _productdbcontext.OrderTbl.Where(item=>item.UserID==id).ToList();
            return orderItems;


        }

        public UserInfo GetUserById(int userID)
        {
            return _productdbcontext.UserTbl.Where(u => u.UserID == userID).FirstOrDefault();
        }
    }
}
