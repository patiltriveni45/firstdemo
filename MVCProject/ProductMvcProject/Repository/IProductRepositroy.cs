﻿using ProductMvcProject.Models;

namespace ProductMvcProject.Repository
{
    public interface IProductRepositroy
    {
        List<ProductDb> ViewProduct();
        ShoppingCart GetCartById(int id);
        void AddToCart(int id, int userID);
        void UpdateQuantity(int id);
        List<ShoppingCart> ViewCart(int userID);
        bool DeleteFromCart(int id);
        void DeleteFromCart(List<ShoppingCart> cart);
        bool AddOrder(List<Order> order);
        List<Order> ViewOrder(int id);
        UserInfo GetUserById(int userID);
    }
}
