﻿using ProductMvcProject.Context;
using ProductMvcProject.Models;

namespace ProductMvcProject.Repository
{
    public class UserRepository:IUserRepository
    {
        Product_DbContext _productdbcontext;
        public UserRepository(Product_DbContext product_DbContext)
        {
            _productdbcontext = product_DbContext;
        }
        public void Registeruser(UserInfo userinfo)
        {
            _productdbcontext.UserTbl.Add(userinfo);
            _productdbcontext.SaveChanges();
        }

        
        public UserInfo GetUserByName(string username)
        {
            return _productdbcontext.UserTbl.Where(u => u.UserName == username).FirstOrDefault();
        }

        public UserInfo GetUserByLogin(string username, string password)
        {
            return _productdbcontext.UserTbl.Where(u => u.UserName == username && u.Password == password).FirstOrDefault();
        }
    }
}
