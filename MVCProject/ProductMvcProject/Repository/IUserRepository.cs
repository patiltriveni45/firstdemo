﻿using ProductMvcProject.Models;

namespace ProductMvcProject.Repository
{
    public interface IUserRepository
    {
        void Registeruser(UserInfo userinfo);
        UserInfo GetUserByName(string username);
        
        UserInfo GetUserByLogin(string username, string password);
    }
}
