﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="WebApplicationDay13.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style2 {
            width: 209px;
        }
        .auto-style3 {
            width: 43%;
        }
        .auto-style4 {
            width: 50px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        &nbsp;<br />
        <br />
        <div>
            <table class="auto-style3">
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label1" runat="server" Text="Username"></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:TextBox ID="txtusername" runat="server"></asp:TextBox>
                    </td>
                    
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:TextBox ID="txtpassword" runat="server"></asp:TextBox>
                    </td>
                    
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Button ID="btnsubmit" runat="server" Text="Submit" />
                    </td>
                    <td class="auto-style2">
                        <asp:Button ID="btncancle" runat="server" Text="Cancle" />
                    </td>
                    
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
