﻿using Microsoft.AspNetCore.Mvc;
using MvC_Core.Context;
using MvC_Core.Models;

namespace MvC_Core.Repository
{
    public class UserRepositorycs : IUserRepository
    {

        //List<Usercs> users; 
        UserDbContext _userDbContext;
        public UserRepositorycs(UserDbContext userDbContext)
        {
            //users = new List<Usercs>();
            _userDbContext=userDbContext;


        }

        public bool AddUser(Usercs user)
        {
            _userDbContext.users.Add(user);
            return _userDbContext.SaveChanges() == 1 ? true : false;
        }

        public bool Delete(int id)
        {
            Usercs user = GetUserById(id);
            _userDbContext.users.Remove(user);
            return _userDbContext.SaveChanges() == 1 ? true : false;
            return true;
        }

        public bool  Edit(Usercs user)
        {
            _userDbContext.Entry<Usercs>(user).State=Microsoft.EntityFrameworkCore.EntityState.Modified;    
            _userDbContext.SaveChanges();
            return true;
        }

        public List<Usercs> GetAllUser()
        {
            return _userDbContext.users.ToList();
        }

        public Usercs GetUserById(int id)
        {
            return _userDbContext.users.Where(u => u.Id == id).FirstOrDefault();
        }

        public Usercs GetUserByName(string name)
        {
            return _userDbContext.users.Where(u=>u.Name == name).FirstOrDefault();
        }
        

    }
}
