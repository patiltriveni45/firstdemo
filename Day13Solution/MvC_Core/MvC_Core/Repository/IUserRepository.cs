﻿using MvC_Core.Models;

namespace MvC_Core.Repository
{
    public interface IUserRepository
    {
        List<Usercs> GetAllUser();
        //void AddUser(Usercs user);
        //void DeleteByElement(int id);
        //Usercs FindElementBId(int id);
        Usercs GetUserByName(string Name);
        bool AddUser(Usercs user);
        Usercs GetUserById(int id);
        bool Delete(int id);
        bool Edit(Usercs user);
    }
}
