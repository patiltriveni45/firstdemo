﻿using Microsoft.AspNetCore.Mvc;
using MvC_Core.Models;
using MvC_Core.Repository;
using MvC_Core.Service;

namespace MvC_Core.Controllers
{
    public class UserController : Controller
    {

        #region old using list
        //UserRepositorycs userRepository = new UserRepositorycs();
        //IUserRepository userRepository;
        //public UserController(IUserRepository userRepositoryy)
        //{
        //    userRepository = userRepositoryy;
        //}
        //public ActionResult GetAlluser()
        //{
        //    List<Usercs> allUser = userRepository.GetAllUser();
        //    return View(allUser);
        //}

        //[HttpGet]
        //public ActionResult Adduser()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Adduser(Usercs user)
        //{
        //    userRepository.AddUser(user);
        //    return RedirectToAction("GetAllUser");
        //}
        //[HttpGet]

        //public ActionResult DeleteElement(int Id)
        //{
        //    return View(userRepository.FindElementBId(Id));
        //}
        //[HttpPost]
        //public ActionResult DeleteElement(int Id, Usercs user)
        //{
        //    userRepository.DeleteByElement(Id);
        //    return RedirectToAction("GetAllUser");
        //}
        //[HttpGet]
        //public ActionResult EditElemnt(int id)
        //{
        //    return View(userRepository.FindElementBId(id));
        //}
        //[HttpPost]
        //public ActionResult EditElemnt(Usercs user)
        //{
        //    DeleteElement(user.Id, user);
        //    Adduser(user);
        //    return RedirectToAction("GetAllUser");

        //}
        //public ActionResult DisplayDetails(int id)
        //{
        //    return View(userRepository.FindElementBId(id));
        //}

        #endregion

        readonly IUserService _userService;
        public UserController(IUserService userService)

        {
            _userService = userService;
        }
        public ActionResult GetAllUser()
        {
            List<Usercs> users = _userService.GetAllUser();
            return View(users);

        }
        [HttpGet]
        public ActionResult AddUser()
        {
            TempData["Alert"] = "Data Successfully Added";
            //List<Usercs> users = _userService.GetAllUser();
            return View();

        }
        [HttpPost]
        public ActionResult AddUser(Usercs user)
        {

            _userService.AddUser(user);
            return RedirectToAction("GetAllUser");
        }
        public ActionResult Delete(int id)
        {

            _userService.Delete(id);
            return RedirectToAction("GetAllUser");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var user = _userService.GetuserById(id);
            return View(user);

        }
        [HttpPost]
        public ActionResult Edit(Usercs user)
        {
            if (ModelState.IsValid)
            {
                _userService.Edit(user);
                return RedirectToAction("GetAllUser");

            }
            
            return View(user);
            
           
        }
        public ActionResult Details(int id)
        {
            return View(_userService.GetuserById(id));

        }

    }
} 

