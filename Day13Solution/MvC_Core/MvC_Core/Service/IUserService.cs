﻿using MvC_Core.Models;

namespace MvC_Core.Service
{
    public interface IUserService
    {
        List<Usercs> GetAllUser();
        bool AddUser(Usercs user);
        bool Delete(int id);
        void Edit(Usercs user);
        Usercs GetuserById(int id);
    }
}
