﻿using MvC_Core.Models;
using MvC_Core.Repository;

namespace MvC_Core.Service
{
    public class Userservise : IUserService
    {
        readonly IUserRepository _userRepository;
        public Userservise(IUserRepository userRepository)

        {
            _userRepository= userRepository;
        }

        public bool AddUser(Usercs user)
        {
            var userpresnt = _userRepository.GetUserByName(user.Name);
            if(userpresnt == null)
            {
                return _userRepository.AddUser(user);
            }
            return false;
        }

        public bool Delete(int id)
        {
           var user_exist= _userRepository.GetUserById(id);
            if (user_exist != null)
            {
                return _userRepository.Delete(id);
            }
            else return false;

        }

        public void Edit(Usercs user)
        {
            _userRepository.Edit(user);
        }

        public List<Usercs> GetAllUser()
        {
            return _userRepository.GetAllUser();
        }

        public Usercs GetuserById(int id)
        {
            return _userRepository.GetUserById(id);
        }
    }
}
