﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductAPI.Model;
using ProductAPI.Repository;

namespace ProductAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        readonly IProductRepository _productRepository;
        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;   
        }
        [Route("GetAllProduct")]
        [HttpGet]
        public ActionResult GetAllProduct()
        {
            List<Product> productlist = _productRepository.GetAllProduct();
            return Ok(productlist);

        }

        [Route("GetProductByID")]
        [HttpGet]
        public ActionResult GetProductByID(int id)
        {
            var getproduct = _productRepository.GetProductByID(id);
            if (getproduct == null)
            {
                return NotFound($"Product Id {id} not found");

            }
            else
            {
                return Ok(getproduct);
            }
        }
        [Route("AddProduct")]
        [HttpPost]
        public ActionResult AddProduct(Product product)
        {
            bool addproducts = _productRepository.AddProduct(product);
            return Created("api/created",addproducts);
        }
        [Route("EditProduct")]
        [HttpPut]
        public ActionResult EditProduct(int id,string name)
        {
            var productexist=_productRepository.GetProductByID(id);
            if(productexist == null)
            {
                return NotFound($"Product Id {id} not found");
            }
            else
            {
                bool editproduct=_productRepository.EditProduct(id, name);
                return Ok(editproduct);
            }
        }
        [Route("DeletePrduct")]
        [HttpDelete]
        public ActionResult DeletePrduct(int id)
        {
            bool deleteproducts = _productRepository.DeleteProduct(id);
            if (deleteproducts)
            {
                return Ok();
            }
            else
            {
                return BadRequest($"Product with {id} not found");
            }
        }
        
       
    }
}
