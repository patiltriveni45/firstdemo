﻿using ProductAPI.Model;

namespace ProductAPI.Repository
{
    public interface IProductRepository
    {
        List<Product> GetAllProduct();
        Product GetProductByID(int id);
        bool AddProduct(Product product);
        bool EditProduct(int id, string name);
        bool DeleteProduct(int id);
        
    }
}
