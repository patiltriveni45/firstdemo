﻿using ProductAPI.Model;

namespace ProductAPI.Repository
{
    public class ProductRepository : IProductRepository
    {
        List<Product>products;
        public ProductRepository()
        {
            products = new List<Product>()
            {
                new Product() { Id = 1, Name = "Shirt",Category="Cloths" },
                new Product(){ Id = 2, Name = "Chair",Category="Furniture" },
                new Product(){ Id = 3, Name = "Watch",Category="Electronics" },

            };
        }

        public bool AddProduct(Product product)
        {
            products.Add(product);
            return true;
        }

        public bool DeleteProduct(int id)
        {
            var product = GetProductByID(id);
            //return productdelete != null ? products.Remove(productdelete) : false;
            if(product != null)
            {
                products.Remove(product);
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool EditProduct(int id, string name)
        {
            var editstatus= products.Find(u => u.Id == id).Name = name;
            if (editstatus !=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Product> GetAllProduct()
        {
            return products;
        }

        public Product GetProductByID(int id)
        {
            return products.Find(u => u.Id == id);
        }

        
    }
}
