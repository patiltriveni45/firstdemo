﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using RabbitMQ.Client;

namespace BookOrder.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        [HttpPost]
        [Route("BookOrder")]
        public ActionResult BookOrder()
        {
            string Bookmessage = "Order Placed Succesfully";
            byte[] bookingMessageInByte = Encoding.UTF8.GetBytes(Bookmessage);
            ConnectionFactory connectionFactory = new ConnectionFactory();
            connectionFactory.Uri = new Uri("amqp://guest:guest@localhost:5672");
            var connection=connectionFactory.CreateConnection();
            var channel = connection.CreateModel();
            channel.ExchangeDeclare("triveni-BookingExchange", ExchangeType.Fanout, true, false);
            channel.BasicPublish("triveni-BookingExchange", "", null, bookingMessageInByte);
            channel.Close();
            connection.Close();
            return Ok("Success");

        }
        [HttpPost]
        [Route("CancleOrder")]
        public ActionResult CancelOrder()
        {
            return Ok("Success");
        }
    }
}
