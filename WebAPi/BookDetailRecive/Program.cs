﻿// See https://aka.ms/new-console-template for more information
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

ConnectionFactory connectionFactory = new ConnectionFactory();
connectionFactory.Uri = new Uri("amqp://guest:guest@localhost:5672");
var connection = connectionFactory.CreateConnection();
var channel = connection.CreateModel();
channel.QueueDeclare("guest-fanout-Queue",true,false,false);
channel.QueueBind("guest-fanout-Queue", "triveni-BookingExchange", "");
var eventBasicConsumer = new EventingBasicConsumer(channel);
eventBasicConsumer.Received += (Sender, EventArgs) =>
{
    var BookingResponseMessage = Encoding.UTF8.GetString(EventArgs.Body.ToArray());
    Console.WriteLine(BookingResponseMessage);
};
channel.BasicConsume("guest-fanout-Queue", true, eventBasicConsumer);
Console.ReadLine();