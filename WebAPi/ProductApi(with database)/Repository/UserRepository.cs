﻿using ProductApi_with_database_.Context;
using ProductApi_with_database_.Model;

namespace ProductApi_with_database_.Repository
{
    public class UserRepository : IUserRepository
    {
        readonly UserDbContext _userDbContext;
        public UserRepository(UserDbContext  userDbContext)
        {
            _userDbContext = userDbContext;
        }



        public int DeleteUSer(UserRegister userExist)
        {
            _userDbContext.Remove(userExist);
            return _userDbContext.SaveChanges();
        }

        public int EditUser(int id,UserRegister user)
        {
            _userDbContext.Entry(user).State=Microsoft.EntityFrameworkCore.EntityState.Modified;
            return _userDbContext.SaveChanges();
        }

        public List<UserRegister> GetAllUser()
        {
            return _userDbContext.UserTbl.ToList();
        }

        public UserRegister GetUserById(int id)
        {
            return _userDbContext.UserTbl.Where(u => u.UserId == id).FirstOrDefault();
        }

        public UserRegister GetUserByName(string name)
        {
            return _userDbContext.UserTbl.Where(u => u.Name == name).FirstOrDefault();
        }

        public UserRegister Login(string username, string password)
        {
            return _userDbContext.UserTbl.Where(u => u.Name == username && u.Password == password).FirstOrDefault();
        }

        public int RegisterUser(UserRegister user)
        {
            _userDbContext.UserTbl.Add(user);
            return _userDbContext.SaveChanges();
        }
    }
}
