﻿using ProductApi_with_database_.Model;

namespace ProductApi_with_database_.Repository
{
    public interface IUserRepository
    {
        List<UserRegister> GetAllUser();
        UserRegister GetUserByName(string name);
        int RegisterUser(UserRegister user);
        UserRegister  GetUserById(int id);
        int DeleteUSer(UserRegister userExist);
        int EditUser(int id,UserRegister user);
        UserRegister Login(string username, string password);
    }
}
