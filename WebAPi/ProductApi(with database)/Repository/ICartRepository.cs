﻿using ProductApi_with_database_.Model;

namespace ProductApi_with_database_.Repository
{
    public interface ICartRepository
    {
        //ShoppingCart GetElementByName(int id,string UserName);
        //void IncreseQunatity(ShoppingCart cart1);
        bool AddOrder(ShoppingCart cart);
        ShoppingCart GetCartById(string productname,string Email);
        bool RemoveCart(int id);
        List<ShoppingCart> ViewCart();
        ShoppingCart UpdateCart(ShoppingCart cart);
    }
}
