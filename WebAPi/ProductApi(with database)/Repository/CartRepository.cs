﻿using ProductApi_with_database_.Context;
using ProductApi_with_database_.Model;

namespace ProductApi_with_database_.Repository
{
    public class CartRepository:ICartRepository
    {
        readonly UserDbContext _userDbContext;
        public CartRepository(UserDbContext userDbContext)
        {
            _userDbContext = userDbContext;
        }

        public bool AddOrder(ShoppingCart cart)
        {
            
            var addcart=_userDbContext.BookTbl.Add(cart);
            _userDbContext.SaveChanges();
            if (addcart != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public ShoppingCart GetCartById(string productname, string Email )
        {
            return _userDbContext.BookTbl.Where(u => u.ProductName==productname && u.UserName==Email).FirstOrDefault();
        }

        //public ShoppingCart GetElementByName(int id ,string UserName)
        //{
        //    return _userDbContext.BookTbl.Where(u => u.ProductName == productName && u.UserName==UserName).FirstOrDefault();

        //}

        

        //public void IncreseQunatity(ShoppingCart cart1)
        //{
        //    cart1.Quantity += 1;
        //    _userDbContext.Entry(cart1).State= Microsoft.EntityFrameworkCore.EntityState.Modified;
        //    _userDbContext.SaveChanges();
        //}

        public bool RemoveCart(int id)
        {
            var deletecart=_userDbContext.BookTbl.Where(x=>x.BookingId==id).FirstOrDefault();
            _userDbContext.BookTbl.Remove(deletecart);
            return _userDbContext.SaveChanges() == 1 ? true : false;
            return true;
        }

        public ShoppingCart UpdateCart(ShoppingCart cart)
        {

            ShoppingCart updatecart = _userDbContext.BookTbl.FirstOrDefault(x => x.BookingId == cart.BookingId);
            updatecart.ProductName = cart.ProductName;
            updatecart.ProductPrice = cart.ProductPrice;
            updatecart.Quantity = cart.Quantity;
            updatecart.Image = cart.Image;
            updatecart.UserName = cart.UserName;
           _userDbContext.SaveChanges();
            return updatecart;
        }

        public List<ShoppingCart> ViewCart(string name)
        {
            return _userDbContext.BookTbl.Where(u => u.UserName == name).ToList();
        }
    }
}
