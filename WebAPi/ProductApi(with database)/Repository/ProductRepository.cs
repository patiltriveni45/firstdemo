﻿using ProductApi_with_database_.Context;
using ProductApi_with_database_.Model;

namespace ProductApi_with_database_.Repository
{
    public class ProductRepository : IProductRepository
    {

        readonly UserDbContext _userDbContext;
        public ProductRepository(UserDbContext userDbContext)
        {
            _userDbContext = userDbContext;
        }

        public int AddProduct(Product product)
        {
            _userDbContext.ProductTbl.Add(product);
            return _userDbContext.SaveChanges();
        }

        public int DeleteUSer(Product ProductExist)
        {
            _userDbContext.ProductTbl.Remove(ProductExist);
            return _userDbContext.SaveChanges();
        }

        public int EditProduct(int id, Product product)
        {
            _userDbContext.Entry(product).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            return _userDbContext.SaveChanges();
        }

        public List<Product> GetAllProduct()
        {
            return _userDbContext.ProductTbl.ToList();
        }

        public Product GetProductById(int id)
        {
            return _userDbContext.ProductTbl.Where(u => u.ProductID==id).FirstOrDefault();
        }

        public Product GetproductByName(string productName)
        {
            return _userDbContext.ProductTbl.Where(u => u.ProductName == productName).FirstOrDefault();
        }
        

        
    }
}
