﻿using ProductApi_with_database_.Model;

namespace ProductApi_with_database_.Repository
{
    public interface IProductRepository
    {
        List<Product> GetAllProduct();
        int AddProduct(Product product);
        Product GetproductByName(string productName);
        Product GetProductById(int id);
        int DeleteUSer(Product ProductExist );
        int EditProduct(int id, Product product);
    }
}
