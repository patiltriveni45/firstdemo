﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductApi_with_database_.Model
{
    public class ShoppingCart
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int BookingId { get; set; }
        public string ProductName { get; set; }
        public int ProductPrice { get; set; }
        public string Image { get; set; }
        public string UserName { get; set; }
        public int Quantity { get; set; }

    }
}
