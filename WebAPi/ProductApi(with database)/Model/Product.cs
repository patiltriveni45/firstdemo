﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductApi_with_database_.Model
{
    public class Product
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
       
        public int ProductID { get; set; }
        public string ProductName { get; set; }

        public string? ProductCategory { get; set; }

        public string? ProductDescription { get; set; }

        public double Price { get; set; }

        public string Image { get; set; }

       

    }
}
