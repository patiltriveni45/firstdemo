﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductApi_with_database_.Model
{
    public class UserRegister
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string  Name { get; set; }

        public string? Email { get; set; }

        public string? Password { get; set; }

        public string Role { get; set; } = "User";



    }
}
