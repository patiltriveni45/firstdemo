﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductApi_with_database_.Exceptions;
using ProductApi_with_database_.Model;
using ProductApi_with_database_.Service;

namespace ProductApi_with_database_.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        readonly IProductService _productService;
        //readonly IWebHostEnvironment webHostEnvironment;
        public ProductController(IProductService productService)
        {
            _productService= productService;

        }

       
        [Route("GetAllProduct")]
        [HttpGet]
        public ActionResult GetAllProduct()
        {
            List<Product> products = _productService.GetAllProduct();
            return Ok(products);
        }
        [Route("AddProduct")]
        [HttpPost]
        public ActionResult AddProduct(Product product)
        {
            try
            {
                bool AddStatus = _productService.AddProduct(product);
                return Ok(AddStatus);
            }
            catch(ProductAlreadyExist pae)
            {
                return BadRequest(pae.Message);
            }
           
        }
        [Route("DeleteProduct")]
        [HttpDelete]
        public ActionResult DeleteProduct(int id)
        {
            try
            {
                bool DeleteStatus = _productService.DeleteProduct(id);
                return Ok(DeleteStatus);
            }
            catch(ProductNotExist pne)
            {
                return BadRequest(pne.Message);
            }
            
        }
        //[Route("UploadFile")]
        //[HttpPost]
        //public ActionResult UploadFile(List<IFormFile>? file)
        //{
        //    if (file?.Count==0)
        //    {
        //        return BadRequest();
        //    }
        //    string? uploadsfolder = Path.Combine(webHostEnvironment?.ContentRootPath,"UploadFiles");
        //    foreach(var files in file)
        //    {
        //    string? filepath = Path.Combine(uploadsfolder,files?.FileName);
        //    using (var stream = new FileStream(filepath, FileMode.Create))
        //        {
        //            files.CopyTo(stream);
        //        }
        //     }
        //     return Ok("Imgae Upload Successfully");
            
            
        //}

        [Route("EditProduct")]
        [HttpPut]
        public ActionResult EditProduct(int id, Product product)
        {
            bool EditStatus = _productService.EditProduct(id, product);
            return Ok(EditStatus);

        }

    }
}
