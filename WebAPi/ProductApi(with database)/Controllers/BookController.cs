﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProductApi_with_database_.HandlerLogic;
using ProductApi_with_database_.Model;
using ProductApi_with_database_.Service;

namespace ProductApi_with_database_.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ExceptionHandler]
    public class BookController : ControllerBase
    {
        private readonly ICartService _cartservice;
        public BookController(ICartService cartservice)
        {
            _cartservice = cartservice;
        }
        [Route("ViewCart")]
        [HttpGet]
        public ActionResult ViewCart(string name)
        {
            List<ShoppingCart> viewCart = _cartservice.ViewCart();
            if (viewCart != null)
            {
                return Ok(viewCart);
            }
            else
            {
                return Ok(false);
            }
        }
        [HttpPost]
        [Route("OrderProduct")]
        public ActionResult AddOrder(ShoppingCart cart)
        {

            bool result = _cartservice.AddOrder(cart);
            if (result)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest("Cart Not Found");
            }

        }
        [HttpDelete]
        [Route("RemoveCart")]
        public ActionResult RemoveCart(int id)
        {
            bool deleteCart=_cartservice.RemoveCart(id);
            if (deleteCart)
            {
                return Ok(JsonConvert.SerializeObject(deleteCart));
            }
            else
            {
                return NotFound("Cart Not Found");
            }
            
        }
        [HttpPut]
        [Route("UpdateCart")]
        public ActionResult UpdateCart(int id, ShoppingCart cart)
        {

            cart.BookingId = id;
            ShoppingCart updateCart = _cartservice.UpdateCart(cart);
            if (updateCart != null)
            {
                return Ok("Cart Updated");
            }
            else
            {
                return BadRequest("Failed to update");
            }



        }

    }
}
