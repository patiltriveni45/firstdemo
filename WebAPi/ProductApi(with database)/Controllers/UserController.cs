﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductApi_with_database_.HandlerLogic;
using ProductApi_with_database_.Model;
using ProductApi_with_database_.Service;

namespace ProductApi_with_database_.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ExceptionHandler]
    public class UserController : ControllerBase
    {
        readonly IUserService _userService;
        readonly ITokenGenrator _tokenGenrator;
        public UserController(IUserService userService,ITokenGenrator tokenGenrator)
        {
            _tokenGenrator = tokenGenrator;
            _userService = userService;
        }
        [Route("GetAllUser")]
        [HttpGet]

        public ActionResult GetAllUser()
        {
            List<UserRegister> users = _userService.GetAllUser();
            return Ok(users);
        }
        [Route("RegisterUser")]
        [HttpPost]
        public ActionResult RegisterUser(UserRegister user)
        {
            bool registerstatus = _userService.RegisterUser(user);
            return Ok(registerstatus);
        }
        [Route("DeleteUser")]
        [HttpDelete]
        public ActionResult DeleteUser(int id)
        {
            bool DeleteStatus = _userService.DeleteUSer(id);
            return Ok(DeleteStatus);
        }

        [Route("EditUser")]
        [HttpPut]
        public ActionResult EditUser(int id,UserRegister user)
        {
            bool EditStatus = _userService.EditUser(id,user);
            return Ok(EditStatus);

        }
        [Route("BlockUnBlocakUser")]
        [HttpPut]

       
        [Route("Login")]
        [HttpPost]
        public ActionResult LoginAsync(UserLogin login)
        {
           UserRegister user= _userService.Login(login);
           string UserToken= _tokenGenrator?.GenerateToken(user);
           return Ok(UserToken);
        }
        
    }


}
