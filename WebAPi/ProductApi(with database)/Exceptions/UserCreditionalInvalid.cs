﻿namespace ProductApi_with_database_.Exceptions
{
    public class UserCreditionalInvalid : ApplicationException
    {
        public UserCreditionalInvalid(string? message) : base(message)
        {
        }
    }
}
