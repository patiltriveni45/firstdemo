﻿namespace ProductApi_with_database_.Exceptions
{
    public class ProductNotExist : ApplicationException
    {
        public ProductNotExist(string? message) : base(message)
        {
        }
    }
}
