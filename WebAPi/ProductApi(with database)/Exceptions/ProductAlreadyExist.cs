﻿namespace ProductApi_with_database_.Exceptions
{
    public class ProductAlreadyExist:ApplicationException
    {
        public ProductAlreadyExist(string v):base(v)
        {
        }
    }
}
