﻿using Microsoft.EntityFrameworkCore;
using ProductApi_with_database_.Model;

namespace ProductApi_with_database_.Context
{
    public class UserDbContext:DbContext
    {
        public UserDbContext(DbContextOptions Context):base(Context)    
        {

        }
        public DbSet<UserRegister> UserTbl { get; set; }
        public DbSet<Product> ProductTbl { get; set; }
        public DbSet<ShoppingCart> BookTbl { get; set; }
      
    }
}
