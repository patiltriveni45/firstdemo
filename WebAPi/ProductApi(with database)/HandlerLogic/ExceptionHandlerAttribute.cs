﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ProductApi_with_database_.Exceptions;

namespace ProductApi_with_database_.HandlerLogic
{
    public class ExceptionHandlerAttribute:ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception.GetType() == typeof(UserCreditionalInvalid))
            {
                context.Result = new OkObjectResult(context.Exception.Message);
            }
            else if (context.Exception.GetType() == typeof(UserAlreayExist))
            {
                context.Result = new OkObjectResult(context.Exception.Message);
            }
            else if (context.Exception.GetType() == typeof(ProductAlreadyExist))
            {
                context.Result = new OkObjectResult(context.Exception.Message);
            }
            else
            {
                context.Result = new StatusCodeResult(500);
            }
        }
    }
}
