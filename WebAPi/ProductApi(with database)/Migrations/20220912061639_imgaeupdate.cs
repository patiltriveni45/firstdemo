﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProductApi_with_database_.Migrations
{
    /// <inheritdoc />
    public partial class imgaeupdate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Image",
                table: "BookTbl",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Image",
                table: "BookTbl");
        }
    }
}
