﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProductApi_with_database_.Migrations
{
    /// <inheritdoc />
    public partial class role : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsBlock",
                table: "UserTbl");

            migrationBuilder.AddColumn<string>(
                name: "Role",
                table: "UserTbl",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Role",
                table: "UserTbl");

            migrationBuilder.AddColumn<bool>(
                name: "IsBlock",
                table: "UserTbl",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
