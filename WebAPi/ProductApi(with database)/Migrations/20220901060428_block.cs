﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProductApi_with_database_.Migrations
{
    /// <inheritdoc />
    public partial class block : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsBlock",
                table: "UserTbl",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsBlock",
                table: "UserTbl");
        }
    }
}
