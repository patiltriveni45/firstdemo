﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using ProductApi_with_database_.Model;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace ProductApi_with_database_.Service
{
    public class TokenGenrator : ITokenGenrator
    {
        private readonly IConfiguration _configuration;
        public TokenGenrator(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string GenerateToken(UserRegister user )
        {
            var Userclaims = new List<Claim>
            {
                new Claim(ClaimTypes.Role,user.Role),
                new Claim(JwtRegisteredClaimNames.Jti,new Guid().ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName,user.Name)
            };
            var userSecurityKey = Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]);
            var userSymmetricSecurity = new SymmetricSecurityKey(userSecurityKey);
            var userSiginCreditional = new SigningCredentials(userSymmetricSecurity, SecurityAlgorithms.HmacSha256Signature);
            var userJwtSecurityToken = new JwtSecurityToken

                (
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    claims: Userclaims,
                    expires: DateTime.UtcNow.AddHours(5),
                    signingCredentials: userSiginCreditional


                );
            var userSecurityTokenHandler = new JwtSecurityTokenHandler().WriteToken(userJwtSecurityToken);
            string userjwttoken=JsonConvert.SerializeObject(new {Token=userSecurityTokenHandler,name= user.Name,Role=user.Role});
            return userjwttoken;

        }
    }
}
