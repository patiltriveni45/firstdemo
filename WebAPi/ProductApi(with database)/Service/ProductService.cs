﻿using ProductApi_with_database_.Exceptions;
using ProductApi_with_database_.Model;
using ProductApi_with_database_.Repository;

namespace ProductApi_with_database_.Service
{
    public class ProductService : IProductService
    {
        readonly IProductRepository _productRepository;
        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public bool AddProduct(Product product)
        {
            var userExist = _productRepository.GetproductByName(product.ProductName);
            if (userExist == null)
            {
                int userRegisterStatus = _productRepository.AddProduct(product);
                if (userRegisterStatus == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                throw new ProductAlreadyExist("Product Already Exist");
            }

        }

        public bool DeleteProduct(int id)
        {
            var  ProductExist = _productRepository.GetProductById(id);
            if (ProductExist != null)
            {
                int ProductDeleteStatus = _productRepository.DeleteUSer(ProductExist);
                if (ProductDeleteStatus == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                throw new ProductNotExist("Product That You Want to delete not exist");
            }
        }

        public bool EditProduct(int id, Product product)
        {
            product.ProductID = id;
            int productStatus = _productRepository.EditProduct(id, product);
            if (productStatus == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
       
        public List<Product> GetAllProduct()
        {
            return _productRepository.GetAllProduct();
        }

    }
}
