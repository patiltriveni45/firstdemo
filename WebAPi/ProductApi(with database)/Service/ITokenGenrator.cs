﻿using ProductApi_with_database_.Model;

namespace ProductApi_with_database_.Service
{
    public interface ITokenGenrator
    {
        string GenerateToken(UserRegister user);
    }
}
