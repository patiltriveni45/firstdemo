﻿using ProductApi_with_database_.Exceptions;
using ProductApi_with_database_.Model;
using ProductApi_with_database_.Repository;

namespace ProductApi_with_database_.Service
{
    public class UserService : IUserService
    {
        readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository=userRepository;
        }

        

        public bool DeleteUSer(int id)
        {
            UserRegister userExist = _userRepository.GetUserById(id);
            if (userExist != null)
            {
                int userDeleteStatus = _userRepository.DeleteUSer(userExist);
                if (userDeleteStatus== 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool EditUser(int id, UserRegister user)
        {
            user.UserId = id;
            int userEditStatus = _userRepository.EditUser(id,user);
            if (userEditStatus ==1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<UserRegister> GetAllUser()
        {
            return _userRepository.GetAllUser();
        }

        public UserRegister Login(UserLogin login)
        {
            UserRegister user = _userRepository.Login(login.Username, login.Password);
            if (user != null)
            {
                return user;
            }
            else
            {
                throw new UserCreditionalInvalid($"User not regitser ");
            }
        }
    
        public bool RegisterUser(UserRegister user)
        {
            var userExist = _userRepository.GetUserByName(user.Name);
            if (userExist == null)
            {
                int userRegisterStatus = _userRepository.RegisterUser(user);
                if (userRegisterStatus == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                throw new UserAlreayExist($" User Already Exist ");
            }
           
        }

    }
}
