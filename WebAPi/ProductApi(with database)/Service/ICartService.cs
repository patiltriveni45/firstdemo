﻿using ProductApi_with_database_.Model;

namespace ProductApi_with_database_.Service
{
    public interface ICartService
    {
        bool AddOrder(ShoppingCart cart);
        bool RemoveCart(int id);
        List<ShoppingCart> ViewCart();
        ShoppingCart UpdateCart(ShoppingCart cart);
    }
}
