﻿using ProductApi_with_database_.Model;

namespace ProductApi_with_database_.Service
{
    public interface IUserService
    {
        List<UserRegister> GetAllUser();
        bool RegisterUser(UserRegister user);
        bool DeleteUSer(int id);
        bool EditUser(int id, UserRegister user);
       
        UserRegister Login(UserLogin login);
    }
}
