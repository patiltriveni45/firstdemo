﻿using ProductApi_with_database_.Exceptions;
using ProductApi_with_database_.Model;
using ProductApi_with_database_.Repository;

namespace ProductApi_with_database_.Service
{
    public class CartService:ICartService
    {
        private readonly ICartRepository _cartrepository;
        public CartService(ICartRepository cartrepository)
        {
            _cartrepository = cartrepository;
        }

        public bool AddOrder(ShoppingCart cart)
        {
            ShoppingCart cart1 = _cartrepository.GetCartById(cart.ProductName, cart.UserName);
            if (cart1 == null)
            {
                _cartrepository.AddOrder(cart);
                return true;
            }
            else
            {
                throw new ProductAlreadyExist("Product Already Exist");
            }



        }



        public bool RemoveCart(int id)
        {
            
          return _cartrepository.RemoveCart(id);
           
        }

        public ShoppingCart UpdateCart(ShoppingCart cart)
        {
            ShoppingCart cartExist = _cartrepository.GetCartById(cart.ProductName, cart.UserName);
            if (cartExist != null)
            {
                return _cartrepository.UpdateCart(cart);


            }
            else
            {

                return null;
            }
        }

        public List<ShoppingCart> ViewCart()
        {
            return _cartrepository.ViewCart();
        }
    }
}
