﻿using ProductApi_with_database_.Model;

namespace ProductApi_with_database_.Service
{
    public interface IProductService
    {
        List<Product> GetAllProduct();
        bool AddProduct(Product product);
        bool DeleteProduct(int id);
        bool EditProduct(int id, Product product);
    }
}
