﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structure
{
    struct Product
    {
        public string ProductName;
        public int ProductPrice;
        public string ProductQuality;

        public Product(string PName,int Pprice ,string Pquality)
        {
            this.ProductName = PName;
            this.ProductPrice = Pprice;
            this.ProductQuality = Pquality;
        }
    }
}
