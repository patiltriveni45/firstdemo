﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structure
{
    struct Address
    {
        public string houseNumber;
        public string city;
        public string locality;

        public Address(string hno,string city,string locality)
        {
            this.houseNumber = hno;
            this.city = city;
            this.locality = locality;
        }
    }
}
