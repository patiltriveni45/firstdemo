﻿// See https://aka.ms/new-console-template for more information
//Console.WriteLine("Hello, World!");

using ClassArray.Model;
using ClassArray.Repository;

ProductRepository pr = new ProductRepository();


Console.WriteLine("******Product Application Using Class Array******");

Console.WriteLine("****Display All Product****\n");

Product[] Allproducts = pr.GetAllproducts();

foreach (Product item in Allproducts)
{
    Console.WriteLine(item);
}
Console.WriteLine("              ");
Console.WriteLine("***********Get Product By Name *********** ");
Console.WriteLine("       ");
Console.WriteLine("Enter Product Name");
string productname=Console.ReadLine();
var a=pr.GetProductByName(productname) ;
foreach (Product p in a)
{
    Console.WriteLine(p);
}
Console.WriteLine("    ");
Console.WriteLine("****Update Product Name ****");
Console.WriteLine("        ");
Console.WriteLine("which product do you want to update ? Please Enter Id");
int inputId = int.Parse(Console.ReadLine());
Console.WriteLine("        ");
Console.WriteLine("Enter new product name");
string val = Console.ReadLine();
var result = pr.UpdateArray(inputId,val);
foreach(var item1 in result)
{
    Console.WriteLine("        ");
    Console.WriteLine("before updated array");
    Console.WriteLine(item1);
    Console.WriteLine("After updated array");
    item1.Name = val;
    Console.WriteLine(item1);
}

Console.WriteLine("        ");
Console.WriteLine("****Delete Product BY ID ****");

Console.WriteLine("enter id to delete");
int delid = int.Parse(Console.ReadLine());
var del = pr.DeleteArray(delid);
foreach (Product item in del)
{
    Console.WriteLine(item);
}

