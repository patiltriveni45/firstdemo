﻿using ClassArray.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassArray.Repository
{
    internal class ProductRepository
    {
        Product[] Products;
        public ProductRepository()
        {
            Products = new Product[4]
            {
                new Product(1,"oppo", "Mobile", 20000, 4.2f),
                new Product(2,"Iphone", "Mobile", 450000, 5.2f),
                new Product(3,"Lenovo", "Laptop", 87000, 3.6f),
                new Product(4,"Samsung",  "Tv",   234000, 4.9f)
            };
        }



        public Product[] GetAllproducts()
        {
            return Products;
        }

        public Product[] GetProductByName(string name)
        {
            return Array.FindAll(Products, Product => Product.Name == name);

        }
        public Product[] DeleteArray(int id)
        {
            return Array.FindAll(Products, Product => Product.Id != id);
        }
        public Product[] UpdateArray(int id,string val)
        {
            return Array.FindAll(Products,pd=>pd.Id == id);
        }

    }
}
