﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassArray.Model
{
    internal class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public int Price { get; set; }
        public float Rating { get; set; }
        public Product(int id, string name, string category, int price, float rating)
        {
            Name = name;
            Category = category;
            Price = price;
            Rating = rating;
            Id = id;
        }
        public override string ToString()
        {
            return $"ID :{Id}\t Name : {Name}\t Category : {Category}\t Price: {Price}\t Rating : {Rating}";
        }
    }
}
