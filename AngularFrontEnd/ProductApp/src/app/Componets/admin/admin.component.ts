import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Product } from 'src/app/Models/product';
import { AdminProductsService } from 'src/app/Service/admin-products.service';
import { ProductService } from 'src/app/Service/product.service';
import Swal from 'sweetalert2';
import { ProductFormComponent } from './product-form/product-form.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {


  constructor(public productservice: AdminProductsService) {
   }
   @ViewChild(ProductFormComponent) product!:ProductFormComponent;
  ngOnInit(): void {
   
    this.productservice.getProduct().subscribe(data=>{
      this.productservice.listproducts=data;
    });
    
  }
  populateEmployee(selecetedEmployee:Product){
    
    
    this.productservice.ProductData=selecetedEmployee;
    console.log(selecetedEmployee);
    if(this.product.isSlide==='off')
    {
     this.product.hideShowSlide();
    }
    

  }
  delete(id:any)
  {
    if(confirm('Are you really want to delete this record?'))
    {
      this.productservice.deleteProduct(id).subscribe(
        data=>{
        console.log(data);
        Swal.fire(
          'Product Deleted Successfully',
          'success'
        )
        
        window.location.reload();
      },
      );
    }
  }


}

