import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Product } from 'src/app/Models/product';
import { AdminProductsService } from 'src/app/Service/admin-products.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  constructor(public productservice: AdminProductsService) { }
  @ViewChild('checkbox1') checkbox!:ElementRef;
  isSlide:string='off';
  unamePattern="^[A-Za-z]{1}[A-Za-z]{4,8}$"
  ngOnInit(): void {
   
  }
  submit(form:NgForm)
  {
    
    if(this.productservice.ProductData.productID==0)
      this.insertEmployee(form);
     else
     this.updateEployee(form);
  }
  insertEmployee(myform:NgForm)
  {  this.productservice.saveProduct().subscribe(d=> {
     this.resetForm(myform);
     this.refereshData();
     Swal.fire(
      'Product Added Successfully',
      'success'
    )
    });
  }
  updateEployee(myform:NgForm)
  {
    this.productservice.updateProduct().subscribe(d=> {
      this.resetForm(myform);
      this.refereshData();
      Swal.fire(
        'Product Update Successfully',
        'success'
      )
      
    });
  }
  resetForm(myform:NgForm)
  {
    myform.form.reset(myform.value);
    this.productservice.ProductData=new Product();
    this.hideShowSlide();
   
  }
  refereshData()
  {
    this.productservice.getProduct().subscribe(res=>{
      this.productservice.listproducts=res;
    });
  }
  hideShowSlide()
  {
    if(this.checkbox.nativeElement.checked)
    {
      this.checkbox.nativeElement.checked=false;
      this.isSlide='off';
    }
    else
    {
      this.checkbox.nativeElement.checked=true;
      this.isSlide='on';
    }
  }

}
