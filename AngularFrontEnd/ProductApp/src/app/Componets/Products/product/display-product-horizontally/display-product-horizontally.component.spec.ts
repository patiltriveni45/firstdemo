import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayProductHorizontallyComponent } from './display-product-horizontally.component';

describe('DisplayProductHorizontallyComponent', () => {
  let component: DisplayProductHorizontallyComponent;
  let fixture: ComponentFixture<DisplayProductHorizontallyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayProductHorizontallyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DisplayProductHorizontallyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
