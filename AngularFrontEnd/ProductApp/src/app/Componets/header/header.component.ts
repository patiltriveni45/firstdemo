import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  DisplayUsername=''
  constructor() { }
  
  ngOnInit(): void {
    
    
  }
  LocalStorage(){
    this.DisplayUsername=localStorage.getItem('UserName')as string;
    return localStorage.getItem('UserName');
    
  }
  GetROle(){
    let roleuser=localStorage.getItem('Role')as string
    if(roleuser=='Admin'){
      return true;
    }
    if(roleuser=='User'){
      return false;
    }
    else{
      return null;
    }
  }
 
  

}
