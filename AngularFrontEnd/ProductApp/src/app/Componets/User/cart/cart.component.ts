import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartItem } from 'src/app/Models/cart-item';
import { CartService } from 'src/app/Service/cart.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartDetails: any=[];
  total? : any = 0;
  constructor(private cartService:CartService, private router:Router) { }

  ngOnInit(): void {
    this.GetCartDetails();
  }
  GetCartDetails() {
    this.cartService.cartDetails().subscribe(res=>
      {
          this.cartDetails = res;
          this.total = this.getPrice(res);
          console.log(res);
      });
  }
  getPrice = (res:CartItem[]) =>{
    let total = 0;
    res.forEach((item : any)=>{
     total += item.quantity * item.productPrice;
    });
    return total
  }
  public updateCartItem(action : string,id : number){
    let item = this.cartDetails.indexOf(this.cartDetails.find((element:any)=>element.bookingId == id));
    if(action == "increment"){
      this.cartDetails[item].quantity += 1;
      this.cartService.updateCartItem(this.cartDetails[item]).subscribe(res=>{
        console.log(res) 
      })
    }else{
      if( this.cartDetails[item].quantity != 1){
        this.cartDetails[item].quantity -= 1;
        this.cartService.updateCartItem(this.cartDetails[item]).subscribe(res=>{ 
          console.log(res)      
        })
      }
    }
    this.cartDetails = [...this.cartDetails];
    this.total = this.getPrice(this.cartDetails);
  }
  DeleteCart(id:any)
  {
    this.cartService.DeleteCart(id).subscribe(res=>
      {
          console.log(res);
          this.total = this.getPrice(this.cartDetails);

          Swal.fire(
            'Item Deleted From Cart!',
            'success'
          )
          this.router.navigate(['product']);
      })
  }
  }
  


