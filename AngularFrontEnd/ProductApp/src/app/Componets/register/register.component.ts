import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { icon } from '@fortawesome/fontawesome-svg-core';
import { User } from 'src/app/Models/user';
import { UserService } from 'src/app/Service/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
 user:User
  constructor(private userservice:UserService ,private route:Router) { 
    console.log("Within Register Component");
    this.user=new User()
    
  }
  exform!: FormGroup;
  ngOnInit(): void {
    // this.registerUser()
    this.exform = new FormGroup({
      'name' : new FormControl(
        null,
        [Validators.required,
          Validators.pattern('^[A-Za-z]{1}[A-Za-z]{4,8}$'),
          Validators.minLength(5),
          Validators.maxLength(7)

        ] ),
      'email' : new FormControl(
        null, 
        [
          Validators.required,
           Validators.email,
           Validators.pattern('^[0-9a-zA-z]+[.+-_$]{0,1}[0-9a-zA-z]+[@][a-zA-z]+[.][a-zA-z]{2,3}$')
          ]),
      'password' : new FormControl(
        null,
        [
          Validators.required,
          Validators.pattern('^[a-zA-z0-9@&*$]{8,}')
        ]),
      
    });
    }
  registerUser() {
    // this.user.Name="Rinku";
    // this.user.Email="Rinku@gmail.com";
    // this.user.Password="Rinku@123";
    this.user.IsBlock=false
    console.log(this.user);
    
    this.userservice.RegisterUser(this.user).subscribe(
      (res)=>{
      // if(res){
        // console.log("Regitser Success");
        Swal.fire(
          'User Resgistration',
          'Registration Success',
          'success'
        )
        this.route.navigate(['login'])
      },
     (e)=>{
      Swal.fire({
        icon: 'error',
        title: 'User Already Exist'
        
      })
    }
        
    )}
  
}

  


function next(next: any, arg1: (res: any) => void, arg2: (e: any) => void) {
  throw new Error('Function not implemented.');
}

