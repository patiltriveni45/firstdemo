import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { Login } from 'src/app/Models/login';
import { UserService } from 'src/app/Service/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
login:Login;
  constructor(private route:Router,private loginservice:UserService) {
    this.login=new Login();
   }
   form!: FormGroup;
  ngOnInit(): void {
    // this.Gotologin();
    this.form = new FormGroup({
      'name' : new FormControl(
        null,
        [Validators.required,
          Validators.pattern('^[A-Za-z]{1}[A-Za-z]{4,8}$'),
          Validators.minLength(5),
          Validators.maxLength(8)
        ] ),
      
      'password' : new FormControl(
        null,
        [
          Validators.required,
          Validators.pattern('^[a-zA-z0-9@&*$]{8,}')
        ]),
      
    });

  }
  Gotologin() {
    console.log(this.login);
    
    // this.login.Username="Manali";
    // this.login.Password="Manali@123";
  
      this.loginservice.Login(this.login).subscribe(
        (res)=>
        {
        if(this.login.username=="Triveni" && this.login.password=="Triveni@45")
        
        {
        console.log(res);
        // Convet from json
        let jsonObject = JSON.stringify(res);
        let jsonToken = JSON.parse(jsonObject);
        console.log(`User token after login::: ${jsonToken["Token"]}`);
        console.log(jsonToken["name"]);
        
        // // localStorage
        localStorage.setItem('UserToken',jsonToken["Token"]);
        localStorage.setItem('UserName',jsonToken['name']); 
        localStorage.setItem('Role',jsonToken['Role']);
        
        Swal.fire(
          'Admin Login',
          'login success',
          'success'
        )
        this.route.navigate(['admin']);
          }
          else{
            console.log(res);
        // Convet from json
        let jsonObject = JSON.stringify(res);
        let jsonToken = JSON.parse(jsonObject);
        console.log(`User token after login::: ${jsonToken["Token"]}`);
        console.log(jsonToken["name"]);
        
        // // localStorage
        localStorage.setItem('UserToken',jsonToken["Token"]);
        localStorage.setItem('UserName',jsonToken['name']); 
        localStorage.setItem('Role',jsonToken['Role']);
        
        Swal.fire(
          'User Login',
          'login success',
          'success'
        )
        this.route.navigate(['product']);
          }
          
        
      },
      (e)=>{
        Swal.fire({
          icon: 'error',
          title: 'User Not Register'
          
        })
        
      }
      )}

}
        
  

