import { Component, OnInit } from '@angular/core';
import { CartItem } from 'src/app/Models/cart-item';
import { Product } from 'src/app/Models/product';
import { ProductService } from 'src/app/Service/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
product?: Product[];
cart:CartItem;
  constructor(private productservice:ProductService) {
  this.cart=new CartItem;
  // this.product=new Product;
   
   }

  ngOnInit(): void {
    this.productservice.GetAllProduct().subscribe(response=>{
      
      this.product=response;
      console.log(response);
      
      
    });
    // this.AddTocart(this.product);
 
 }
 AddToCart(product:Product)
  {
    this.cart.productName= product.productName;
    this.cart.productPrice= product.price;
    this.cart.userName = localStorage.getItem('UserName') as string;
    this.cart.quantity = 1;
    this.cart.image = product.image;
    console.log(this.cart);
    this.productservice.AddToCart(this.cart).subscribe(
      (res)=>{
      
        // console.log("Product Added to the cart");
        Swal.fire(
          'Added Successfully',
          'success'
        )
      },
      (e)=>{
        Swal.fire({
          icon: 'error',
          title: 'Product Already Exist'
          
        })
      }
    );

}
 
 }




