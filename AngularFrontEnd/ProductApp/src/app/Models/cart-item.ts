
export class CartItem {
  bookingId?: number;
  productName?: string;
  productPrice?: number;
  image?:string;
  userName?: string;
  quantity?: number;


}
