export class Product {
    productID:number=0;
    productName?:string;
    productCategory?:string;
    productDescription?:string;
    price?:number;
    image?:string;
}

