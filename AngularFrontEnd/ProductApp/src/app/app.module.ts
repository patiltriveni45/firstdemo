import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './Componets/register/register.component';
import {HttpClient, HttpClientModule,HttpInterceptor, HTTP_INTERCEPTORS} from '@angular/common/http';
import { ProductComponent } from './Componets/product/product.component';
import { LoginComponent } from './Componets/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './Componets/nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { HeaderComponent } from './Componets/header/header.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {TokenInterceptorService} from './token-interceptor.service';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { AddProductComponent } from './Componets/Products/add-product/add-product.component';
import { DisplayProductHorizontallyComponent } from './Componets/Products/product/display-product-horizontally/display-product-horizontally.component';
import { DisplayProductVerticallyComponent } from './Componets/Products/product/display-product-vertically/display-product-vertically.component';
import { AdminComponent } from './Componets/admin/admin.component';
import { CartComponent } from './Componets/User/cart/cart.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FooterComponent } from './Componets/footer/footer.component';
import {RouterModule} from '@angular/router';
import { ProductFormComponent } from './Componets/admin/product-form/product-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './Componets/home/home.component';



// import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    AppComponent,
   RegisterComponent,
   ProductComponent,
   LoginComponent,
   NavComponent,
   HeaderComponent,
   AddProductComponent,
   DisplayProductHorizontallyComponent,
   DisplayProductVerticallyComponent,
   AdminComponent,
   CartComponent,
   FooterComponent,
   ProductFormComponent,
   HomeComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatButtonModule,
    MatExpansionModule,
    MatFormFieldModule,
    FormsModule,MatInputModule, FontAwesomeModule,
    RouterModule,
    ReactiveFormsModule ,
    

  ],
  providers: [AuthService ,AuthGuard ,{
    provide:HTTP_INTERCEPTORS,
    useClass:TokenInterceptorService,
    multi:true
  }
    
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
