import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './auth.guard';
import { AdminComponent } from './Componets/admin/admin.component';
import { HomeComponent } from './Componets/home/home.component';
import { LoginComponent } from './Componets/login/login.component';
import { LogoutComponent } from './Componets/logout/logout.component';
import { ProductComponent } from './Componets/product/product.component';
import { RegisterComponent } from './Componets/register/register.component';
import { CartComponent } from './Componets/User/cart/cart.component';

const routes: Routes = [
  
  {path:'register',component:RegisterComponent},
  {path:'login',component:LoginComponent},
  {path:'product',component:ProductComponent,canActivate:[AuthGuard]},
  {path:'logout',component:LogoutComponent},
  {path:'admin',component:AdminComponent,canActivate:[AuthGuard]},
  {path:'cart',component:CartComponent,canActivate:[AuthGuard]},
  {path:'root',component:AppComponent,canActivate:[AuthGuard]},
  {path:'',component:HomeComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
