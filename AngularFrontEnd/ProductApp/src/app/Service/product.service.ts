import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CartItem } from '../Models/cart-item';
import { Product } from '../Models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  
  private url = "https://localhost:7058/api/Product/GetAllProduct";
  constructor(private httpClient:HttpClient) {}
  GetAllProduct():Observable<Product[]>{
    return this.httpClient.get<Product[]>(this.url)

  }
  AddToCart(cart: CartItem):Observable<boolean> {
    return this.httpClient.post<boolean>('https://localhost:7058/api/Book/OrderProduct',cart)
}
}
