import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../Models/product';

@Injectable({
  providedIn: 'root'
})
export class AdminProductsService {

  private baseUrl="https://localhost:7058/api/Product"
  private AddProductUrl = "https://localhost:7058/api/Product/AddProduct";
  private EditProductUrl="https://localhost:7058/api/Product/EditProduct";
  private GetProductUrl="https://localhost:7058/api/Product/GetAllProduct";
  // private DeleteProductUrl="https://localhost:7058/api/Product/DeleteProduct?id=";
  constructor(private httpClient:HttpClient) { }
  public listproducts:Product[]=[]; 
  ProductData:Product=new Product();
  saveProduct()
  {
    return this.httpClient.post(this.AddProductUrl,this.ProductData);
  }
  updateProduct()
  {
    return this.httpClient.put("https://localhost:7058/api/Product/EditProduct?id="+this.ProductData.productID,this.ProductData);
  }
  
  getProduct():Observable<Product[]>
  {
    return this.httpClient.get<Product[]>(this.GetProductUrl);
  }
  deleteProduct(id:any):Observable<Boolean>
  {
    return this.httpClient.delete<Boolean>("https://localhost:7058/api/Product/DeleteProduct?id="+id);
  }
}
