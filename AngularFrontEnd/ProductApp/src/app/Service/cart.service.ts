import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CartItem } from '../Models/cart-item';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private httpclient:HttpClient) { }
  cartDetails():Observable<CartItem[]> {
    return this.httpclient.get<CartItem[]>('https://localhost:7058/api/Book/ViewCart');
 }

 DeleteCart(id:any):Observable<Boolean> {
   return this.httpclient.delete<Boolean>('https://localhost:7058/api/Book/RemoveCart?id='+id);
 }

 updateCartItem(cartItem:CartItem):Observable<CartItem> {
   return this.httpclient.put<CartItem>('https://localhost:7058/api/Book/UpdateCart?id='+cartItem.bookingId,cartItem);
 }
}
