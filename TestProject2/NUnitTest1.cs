namespace TestProject2
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var temp1 = 40;
            var expected = 104;
            var actual = TempConverter.celciustofarenheit(temp1);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Test2()
        {
            var temp1 = 40;
            var expected = 14.4;
            var actual = TempConverter.FarenheitToCelcius(temp1);
            Assert.AreEqual(expected, actual);
        }

    }
}