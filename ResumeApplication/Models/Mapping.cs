﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ResumeApplication.Models
{
    public class Mapping
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int Id { get; set; }

        public int JobTypeId { get; set; }


        [ForeignKey("JobTypeId")]

        public virtual JobType jobtype { get; set; }

        public int UserID { get; set; }

        [ForeignKey("UserID")]

        public virtual User user { get; set; }

    }
}
