﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static ResumeApplication.Controllers.UserController;

namespace ResumeApplication.Models
{
    public class User
    {
        #region LoginDetails
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int UserID { get; set; }
        [Required(ErrorMessage ="Email Address is Required")]
        [Remote("IsEmailExist", "User", HttpMethod = "POST", ErrorMessage = "Email Already Exist")]
        [StringLength(40)]
        [Display(Name = "Email Address")]
        [RegularExpression(@"^[0-9a-zA-z]+[.+-_$]{0,1}[0-9a-zA-z]+[@][a-zA-z]+[.][a-zA-z]{2,3}$", ErrorMessage = "Please Enter Valid Email")]
        public string? Email { get; set; }

        [StringLength(40)]
        [Display(Name = "Alternate Email Id")]
        [RegularExpression(@"^[0-9a-zA-z]+[.+-_$]{0,1}[0-9a-zA-z]+[@][a-zA-z]+[.][a-zA-z]{2,3}$", ErrorMessage = "Please Enter Valid Email")]
        public string? AlternateEmail { get; set; }


        [Required(ErrorMessage = "First Name is required")]
        [StringLength(10, MinimumLength = 3, ErrorMessage = "Maximum 10 characters")]
        [Display(Name ="First Name")]
        [RegularExpression(@"^[a-zA-z]+$",ErrorMessage="First Name Should Contain only Alphabet")]
        public string? FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [StringLength(10, MinimumLength = 5, ErrorMessage = "Maximum 10 characters")]
        [Display(Name = "Last Name")]
        [RegularExpression(@"^[a-zA-z]+$", ErrorMessage = "Last Name Should Contain only Alphabet")]
        public string? LastName { get; set; }

        [Required]
        [RegularExpression(@"^[a-zA-z0-9@&*$]{8,}",ErrorMessage = "password pattern not match")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string? Password { get; set; }

        [NotMapped]
        [Required]
        [RegularExpression(@"^[a-zA-z0-9@&*$]{8,}", ErrorMessage = "password pattern not match")]
        [DataType(DataType.Password)]
        [Compare("Password",ErrorMessage = "password and confirmation password do not match")]
        [Display(Name = "Re-Enter Password")]
        public string? ReEnterpassword { get; set; }

        [Required(ErrorMessage ="Please Select option")]
        [Display(Name ="Looking For")]
        public string? Jobtype { get; set; }

        //[NotMapped]
        //public string[]? val { get; set; }

        [Required(ErrorMessage = "Please Select User Status")]
        [StringLength(40)]
        [Display(Name ="Your Status")]
        public string? UserStatus { get;set; }
        #endregion

        #region Personal Details

        [Required]
        [Remote("IsMobileExist", "User", HttpMethod = "POST", ErrorMessage = "Mobile Number Already Exist")]
        [StringLength(10)]
        [RegularExpression(@"^[6-9]{1}[0-9]{9}$",ErrorMessage ="Please Enter Valid Phone Number")]
        [Display(Name ="Phone Number")]
        public string? PhoneNumber { get; set; }

        [StringLength(10)]
        [RegularExpression(@"^[6-9]{1}[0-9]{9,13}$", ErrorMessage = "Please Enter Valid Phone Number")]
        [Display(Name = "Mobile Number")]
        public string? Mobile { get; set; }

        [Required]
        [Display(Name ="Date Of Birth")]
        [DataType(DataType.Date)]
        [Remote("DateValidation","User",HttpMethod="POST",ErrorMessage ="Minimum Age Should greater than 18")]
        public DateTime? DateOfBirth { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public string? Gender { get; set; }

        [Required]
        [StringLength(40)]
        [Display(Name = "Marital Status")]
        public string? MaritalStatus { get; set; }

        [Required(ErrorMessage = "Please Select Country")]
        [Display(Name = "Country")]
        public int CountryId { get; set; }


        [ForeignKey("CountryId")]
        
        public virtual Country country { get; set; }

        [Required(ErrorMessage = "Please Select State")]
        [Display(Name = "State")]
        public int StateId { get; set; }
        
        [ForeignKey("StateId")]
        public virtual State state { get; set; }

        [RegularExpression(@"^[a-zA-z]+$", ErrorMessage = "Letter only Allowed")]
        [Display(Name ="District")]
        public string? District { get; set; }


        //public int hdnCityID { get; set; }

        [Required(ErrorMessage ="Please Select City")]
        [Display(Name = "City")]
        public int CityId{ get; set; }

        
        [ForeignKey("CityId")]
        public virtual City city { get; set; }

        [Required(ErrorMessage = "Locality is Required")]
        [StringLength(10)]
        [RegularExpression(@"^[a-zA-z]+$", ErrorMessage = "Letter only Allowed")]
        public string ? Locality { get; set; }

        [StringLength(6)]
        [RegularExpression(@"^\d{6}$", ErrorMessage = "Please Enter 6 digit zip code")]
        [Display(Name ="Zip Code")]
        public string? ZipCode { get; set; }

        #endregion Personal Details

        #region Education Qualification
        [Required]
        [StringLength(30)]
        [Display(Name ="Highest Qualification")]
        public string? HighestQualification { get; set; }

        [Required]
        [StringLength(30)]
        [Display(Name = "Specialization")]
        public string ? Specialization { get; set; }


        [Required]
        [StringLength(30)]
        [Display(Name = "Institue")]
        public string ? Institue { get; set; }

       
        [StringLength(30)]
        [Display(Name = "Other  Institue")]
        [RegularExpression(@"^[a-zA-z]+$", ErrorMessage = "Letter only Allowed")]
        public string? OtherInstitue { get; set; }

        #endregion

        #region Professional Details
        [StringLength(10)]
        [RegularExpression(@"^[a-zA-z]+$", ErrorMessage = "Letter only Allowed")]
        [Display(Name ="Preferred Location")]
        public string? PreferedLocation { get; set; }

        [Required]
        [Display(Name = "Are you Ready to relocate")]
        public bool ReadyToRelocate { get; set; } = false;

        [Required]
        [Display(Name ="Total Experience")]
        [RegularExpression(@"^[0-9]+$",ErrorMessage = "Number only Allowed")]
        public int TotalExperience { get; set; }

        [Required]
        [Display(Name = "Job Category")]
        [StringLength(50)]
        public string? JobCategory { get; set; }

        [Required]
        [Display(Name = "Key Skills")]
        [StringLength(20)]
        public string? KeySkills { get; set; }

        [StringLength(20)]
        [Display(Name = "Current Industry")]
        [RegularExpression(@"^[a-zA-z]+$", ErrorMessage = "Please Enter Valid Industry Name")]
        public string? CurrentIndustry { get; set; }

        [StringLength(20)]
        [Display(Name = "Current Employer")]
        [RegularExpression(@"^[a-zA-z]+$", ErrorMessage ="Please Enter Valid Employer")]
        public string? CurrentEmployer { get; set; }

        [StringLength(20)]
        [Display(Name = "Current Designation")]
        [RegularExpression(@"^[a-zA-z]+$", ErrorMessage = "Please Enter Valid Designation")]
        public string? CurrentDesignation { get; set; }

        [StringLength(20)]
        [Display(Name = "Previous Employer")]
        [RegularExpression(@"^[a-zA-z]+$", ErrorMessage = "Please Enter Valid Employer")]
        public string? PreviousEmployer { get; set; }

        [StringLength(20)]
        [Display(Name = "Previous Designation")]
        [RegularExpression(@"^[a-zA-z]+$", ErrorMessage = "Please Enter Valid Designation")]
        public string? PreviousDesignation { get; set; }

        [Display(Name = "Notice Period")]
        [DefaultValue("Immedietly")]
        public string? NoticePeriod { get; set; }

        [DefaultValue(0)]
        [Display(Name = "CTC (per annum)")]
        public int CtcNumber { get; set; } = 0;

        [Required]
        [StringLength(20)]
        [Display(Name = "Resume Title")]
        [RegularExpression(@"^[a-zA-z]+$", ErrorMessage = "Letter only Allowed")]
        public string? ResumeTitle { get; set; }

        [Display(Name = "Resume Path")]
        public string? ResumePath { get; set; }

        [NotMapped]
        [Display(Name = "Upload Resume")]
        public IFormFile ResumeFile { get; set; }

        [StringLength(20)]
        [Display(Name = "Type Resume")]
        [RegularExpression(@"^[a-zA-z]+$", ErrorMessage = "Letter only Allowed")]
        public string? Resume { get; set; }


        #endregion

        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public bool IsDeleted { get; set; } = false;
        public DateTime DeletedOn { get; set; } = DateTime.Now;











    }
}
