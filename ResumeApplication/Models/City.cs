﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ResumeApplication.Models
{
    public class City
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int CityId { get; set; }
        public string CityName { get; set; }

        public int StateId { get; set; }

        [ForeignKey("StateId")]
        public virtual State state { get; set; }
    }
}
