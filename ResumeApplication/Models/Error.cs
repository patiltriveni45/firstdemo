﻿namespace ResumeApplication.Models
{
    public class Error
    {
        public int StatusCode { get; set; }
        public string ?Source { get; set; }

        public string ErrorMessage { get; set; }

        public string StackTrace { get; set; }

        public DateTime Datetime { get; set; } = DateTime.Now;

        public string ErrorDetails()
        {
            string errorDetails = $"StatusCode : this.StatusCode\nSource : this.Source\nError Message : this.ErrorMessage\nDatetime : this.Datetime\nStackTrace:this.StackTrace";
            return errorDetails;
        }
    }
}
