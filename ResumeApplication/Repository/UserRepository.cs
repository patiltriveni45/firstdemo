﻿using ResumeApplication.Context;
using ResumeApplication.Models;
using System.Web.Helpers;

namespace ResumeApplication.Repository
{
    public class UserRepository:IUserRepository
    {
        readonly UserDbContext _userDbContext;
        public UserRepository(UserDbContext userDbContext)
        {
            _userDbContext=userDbContext;
        }

        public void RegisterUser(User user)
        {
            user.Password = Crypto.HashPassword(user.Password);
            _userDbContext.UserRegister.Add(user);
            _userDbContext.SaveChanges();
        }
        public List<Country> GetAllCountry()
        {
           return _userDbContext.Counry.ToList();
        }

        public List<State> GetAllState(int CountryId)
        {
           return _userDbContext.State.Where(u => u.CountryId == CountryId).ToList();
        }

        public List<City> GetAllCity(int StateId)
        {
            return _userDbContext.City.Where(u=>u.StateId== StateId).ToList();
        }

        public User GetuserByEmail(string email)
        {
            return _userDbContext.UserRegister.Where(u => u.Email == email).FirstOrDefault();
        }

        public User FindMobileNumber(string? phone)
        {
            return _userDbContext.UserRegister.Where(u => u.PhoneNumber == phone).FirstOrDefault();
        }

        public List<User> GetAllUser()
        {
            return _userDbContext.UserRegister.ToList();
        }
    }
}
