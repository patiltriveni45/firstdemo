﻿using ResumeApplication.Models;

namespace ResumeApplication.Repository
{
    public interface IUserRepository
    {
        void RegisterUser(User user);
        List<Country> GetAllCountry();
        List<State> GetAllState(int CountryId);
        List<City> GetAllCity(int StateId);
        User GetuserByEmail(string email);
        User FindMobileNumber(string phone);
        List<User> GetAllUser();
    }
}
