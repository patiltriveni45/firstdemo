﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ResumeApplication.Models;
using ResumeApplication.Service;
using System.Data;
using System.Net;
using System.Net.Mail;


namespace ResumeApplication.Controllers
{
    public class UserController : Controller
    {
        readonly IUserService _userservice;
        readonly IWebHostEnvironment _webHostEnvironment;
        readonly IConfiguration _config;
        private readonly ILogger<UserController> _logger;
        public UserController(IUserService userService, IWebHostEnvironment webHost, IConfiguration config, ILogger<UserController> logger)
        {
            _userservice = userService;
            _webHostEnvironment = webHost;
            _config = config;
            _logger = logger;   
        }
        [HttpGet]
        public ActionResult RegisterUser()
        {
            //throw new Exception("Error in Register Page");
            int[] years = Enumerable.Range(1, 20).ToArray();
            var yearData = years.Select((i) => new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            ViewBag.YearList = yearData;

            int[] months = Enumerable.Range(2, 11).ToArray();
            var monthData = months.Select((i) => new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            ViewBag.MonthList = monthData;

            int[] lakhs = Enumerable.Range(2, 99).ToArray();
            var lakhData = lakhs.Select((i) => new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            ViewBag.LakhList = lakhData;

            int[] thousands = Enumerable.Range(2, 99).ToArray();
            var thousandData = thousands.Select((i) => new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            ViewBag.ThousandList = thousandData;
            Country_Bind();
            return View();
        }
        [HttpPost]
        public ActionResult RegisterUser(User user,bool readytorelocate, string Years, string Months, string Lakhs, string Thousands,string JobTypes)
        {

                user.TotalExperience = (Convert.ToInt32(Years) * 12) + Convert.ToInt32(Months);
                user.CtcNumber = ((Convert.ToInt32(Lakhs) * 100) * 1000) + (Convert.ToInt32(Thousands) * 1000);
                user.ReadyToRelocate = Convert.ToBoolean(readytorelocate);
                user.Jobtype = JobTypes;

                string? wwwRootPath = _webHostEnvironment.WebRootPath;
                string? filename = Path.GetFileNameWithoutExtension(user.ResumeFile.FileName);
                string? extention = Path.GetExtension(user.ResumeFile.FileName);
                user.ResumePath = filename = filename + Guid.NewGuid().ToString() + "_" + extention;
                string path = Path.Combine(wwwRootPath + _config.GetSection("Resume").GetSection("Resume").Value + "/Resume/", filename);
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    user.ResumeFile.CopyTo(fileStream);
                }
                _userservice.RegisterUser(user);
                TempData["success"] = " Registered Successfully! ";
                SendActivationEmail(user);
               
                return RedirectToAction("RegisterUser");
                
       

        }
        [HttpPost]
        public JsonResult DateValidation(DateTime DateOfBirth)
        {
            DateTime today=DateTime.Now;
            int age = today.Year - DateOfBirth.Year;
            if (age < 18)
            {
                return Json(false);
            }
            else
            {
                return Json(true);
            }
        }
        [HttpPost]
        public JsonResult IsEmailExist(string email)
        {
            var user = _userservice.GetuserByEmail(email);
            return Json(user == null);
        }
        [HttpPost]
        public JsonResult IsMobileExist(string phone)
        {

            var user1 = _userservice.FindMobileNumber(phone);
            //if (user1 == null)
            //{
            //    return Json(true);
            //}
            //else
            //{
            //    return Json(false);
            //}
            return Json(user1 == null);
        }
        //bind country
        public void Country_Bind()
        {

            List<Country> getallcountry = _userservice.GetAllCountry();
            List<SelectListItem> countrylist = new List<SelectListItem>();
            foreach (Country item in getallcountry)
            {

                countrylist.Add(new SelectListItem { Text = item.CountryName.ToString(), Value = item.CountryId.ToString() });

            }
            ViewBag.Country = countrylist;
            

        }
        //State bind
        [HttpGet]
        [Route("User/State_Bind")]
        public JsonResult State_Bind(int CountryId)
        {
            List<State> getallstate = _userservice.GetAllState(CountryId);
            List<SelectListItem> statelist = new List<SelectListItem>();
            foreach (State item in getallstate)
            {

                statelist.Add(new SelectListItem { Text = item.StateName.ToString(), Value = item.StateId.ToString() });

            }
            return Json(statelist);

        }
        //City bind
        [HttpGet]
        [Route("User/City_Bind")]
        public JsonResult City_Bind(int StateId)
        {
            List<City> getallcity = _userservice.GetAllCity(StateId);
            List<SelectListItem> citylist = new List<SelectListItem>();
            foreach (City item in getallcity)
            {

                citylist.Add(new SelectListItem { Text = item.CityName.ToString(), Value = item.CityId.ToString() });

            }
            return Json(citylist);
        }
        private void SendActivationEmail(User user)
        {

            using (MailMessage mm = new MailMessage("patiltriveni45@gmail.com", user.Email))
            {
                mm.Subject = "Registered Successfully!";
                string body = "Dear " + user.FirstName + ",";
                body += "<br /> You application registered successfully on portal.";
                body += "<br /><br /> we will contact you soon!.";
                body += "<br /><br />Thanks"+",";
                body += "<br />Team support";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential("patiltriveni45@gmail.com", "sdovhdzjesfrxyxm");
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }
        [HttpGet]
        public ActionResult GetAllUser()
        {
            List<User> users = _userservice.GetAllUser();
            return View(users);
        }
        [HttpGet]
        public ActionResult DownloadResume(string file)
        {
            string path = Path.Combine(_config.GetValue<string>("Resume:ResumePath"),file);
            var filepath = new FileStream(path, FileMode.Open);
            return File(filepath, "application/octet-stream", file);
        }
      
    }
}
