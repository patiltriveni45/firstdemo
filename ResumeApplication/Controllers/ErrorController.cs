﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using ResumeApplication.Models;
using Serilog;
using System.Diagnostics;
using System.Net;

namespace ResumeApplication.Controllers
{
    public class ErrorController : Controller
    {
        [AllowAnonymous]
        [Route("Error")]
        public IActionResult Error()
        {
            var exception = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var statusCode = exception.Error.GetType().Name switch
            {
                "ArgumentException" => HttpStatusCode.BadRequest,
                _ => HttpStatusCode.ServiceUnavailable
            };
            Log.Logger = new LoggerConfiguration()
           .MinimumLevel.Debug()
           .WriteTo.Console()
           .WriteTo.File("C:\\DOTNET_DEMO\\ResumeApplication\\logs\\myapp.txt", rollingInterval: RollingInterval.Day)
           .CreateLogger();
            
            Error e = new Error() { StatusCode = (int)statusCode, Source = exception.Error.Source, ErrorMessage = exception.Error.Message, Datetime=DateTime.Now ,StackTrace = exception.Error.StackTrace };
            //Log.Error(exception.Error.Message);

            //Log.Error(e.ErrorDetails());

            Log.Error(e.Source);
            Log.Error(e.ErrorMessage);
            Log.Error(e.StackTrace);

            return Json(e);
           
            //return Problem(detail: exception.Error.Message, statusCode: (int)statusCode);
        }
    }
}
