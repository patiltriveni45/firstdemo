﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ResumeApplication.Models;
using ResumeApplication.Repository;

namespace ResumeApplication.Service
{
    public class UserService: IUserService
    {
        readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public List<City> GetAllCity(int StateId)
        {
            return _userRepository.GetAllCity(StateId);
        }

        public List<Country> GetAllCountry()
        {
            return _userRepository.GetAllCountry();
        }

        public List<State> GetAllState(int CountryId)
        {
            return _userRepository.GetAllState(CountryId);
        }

        public List<User> GetAllUser()
        {
            return _userRepository.GetAllUser();
        }

        public User GetuserByEmail(string email)
        {
            return _userRepository.GetuserByEmail(email);
        }

        public User FindMobileNumber(string phone)
        {
            return _userRepository.FindMobileNumber(phone);
        }

        public void RegisterUser(User user)
        {
            _userRepository.RegisterUser(user);
        }
        
    }
}
