﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ResumeApplication.Models;

namespace ResumeApplication.Service
{
    public interface IUserService
    {
        void RegisterUser(User user);
        List<Country> GetAllCountry();
        List<State> GetAllState(int CountryId);
        List<City> GetAllCity(int StateId);
        User GetuserByEmail(string email);
        User FindMobileNumber(string phone);
        List<User> GetAllUser();
    }
}
