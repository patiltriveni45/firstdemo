﻿using Microsoft.EntityFrameworkCore;
using ResumeApplication.Models;

namespace ResumeApplication.Context

{
    public class UserDbContext:DbContext
{
    public UserDbContext(DbContextOptions<UserDbContext> Context) : base(Context)
    {

    }

        public DbSet<User> UserRegister { get; set; }

        public DbSet<State> State { get; set; }

        public DbSet<City> City { get; set; }

        public DbSet<Country> Counry{ get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            foreach (var foreignKey in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
            }

        }

    }
    
}

