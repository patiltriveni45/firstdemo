﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ResumeApplication.Migrations
{
    /// <inheritdoc />
    public partial class first : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Counry",
                columns: table => new
                {
                    CountryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Counry", x => x.CountryId);
                });

            migrationBuilder.CreateTable(
                name: "State",
                columns: table => new
                {
                    StateId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StateName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_State", x => x.StateId);
                    table.ForeignKey(
                        name: "FK_State_Counry_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Counry",
                        principalColumn: "CountryId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "City",
                columns: table => new
                {
                    CityId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CityName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    StateId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_City", x => x.CityId);
                    table.ForeignKey(
                        name: "FK_City_State_StateId",
                        column: x => x.StateId,
                        principalTable: "State",
                        principalColumn: "StateId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserRegister",
                columns: table => new
                {
                    UserID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: false),
                    AlternateEmail = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Jobtype = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    UserStatus = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Mobile = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateOfBirth = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    MaritalStatus = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    StateId = table.Column<int>(type: "int", nullable: false),
                    District = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CityId = table.Column<int>(type: "int", nullable: false),
                    Locality = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    ZipCode = table.Column<int>(type: "int", maxLength: 6, nullable: false),
                    HighestQualification = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    Specialization = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    Institue = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    OtherInstitue = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    PreferedLocation = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    ReadyToRelocate = table.Column<bool>(type: "bit", maxLength: 30, nullable: false),
                    TotalExperience = table.Column<int>(type: "int", nullable: false),
                    JobCategory = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: false),
                    KeySkills = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: false),
                    CurrentIndustry = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CurrentEmployer = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CurrentDesignation = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PreviousEmployer = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PreviousDesignation = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    NoticePeriod = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CtcNumber = table.Column<int>(type: "int", nullable: false),
                    ResumeTitle = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: false),
                    ResumePath = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Resume = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeletedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedOn = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRegister", x => x.UserID);
                    table.ForeignKey(
                        name: "FK_UserRegister_City_CityId",
                        column: x => x.CityId,
                        principalTable: "City",
                        principalColumn: "CityId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserRegister_Counry_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Counry",
                        principalColumn: "CountryId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserRegister_State_StateId",
                        column: x => x.StateId,
                        principalTable: "State",
                        principalColumn: "StateId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_City_StateId",
                table: "City",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_State_CountryId",
                table: "State",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRegister_CityId",
                table: "UserRegister",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRegister_CountryId",
                table: "UserRegister",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRegister_StateId",
                table: "UserRegister",
                column: "StateId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserRegister");

            migrationBuilder.DropTable(
                name: "City");

            migrationBuilder.DropTable(
                name: "State");

            migrationBuilder.DropTable(
                name: "Counry");
        }
    }
}
