﻿// See https://aka.ms/new-console-template for more information
using System.Text;

DateTime startime;
DateTime endime;
TimeSpan TotalTime;
string s1 = "";
startime = DateTime.Now;
for (int i= 0;i< 10000; i++){
    s1+= i.ToString();
}
endime= DateTime.Now;
TotalTime=startime-endime;
Console.WriteLine($"total time taken by string concat :{TotalTime}");

StringBuilder sb = new StringBuilder();
//string s = "";
startime = DateTime.Now;
for (int i = 0; i < 10000; i++)
{
    sb.Append(i.ToString());
}
endime = DateTime.Now;
TotalTime = startime-endime;
Console.WriteLine($"total time taken by string builder :{TotalTime}");

