﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartialClass
{
    internal partial class User
    {
        public string name { get; set; } = "Triveni";
        public int id { get; set; } = 10;
    }
}
