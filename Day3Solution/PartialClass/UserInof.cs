﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartialClass
{
    internal partial class User
    {
        public void GetUserinfo()
        {
            Console.WriteLine($"Name:{name}\t Id :{id}");
        }
    }
}
