﻿using WebApp.Models;

namespace WebApp.Repository
{
    public class UserRepository: IuserRepository
    {
        List<User> users;
        public UserRepository()
        {
            users = new List<User>();

        }
        public List<User> GetAllUser()
        {
            return users;
        }
        public void AddUser(User user)
        {
            users.Add(user);
        }

    }
}
