﻿// See https://aka.ms/new-console-template for more information


using ContactList.Exception;
using ContactList.Model;
using ContactList.Repository;
Contact contact = new Contact();

Console.WriteLine("******Contact Application Using List******");

ContactRepository contactrepo = new ContactRepository();


bool isContinue = true;
while (isContinue)
{
    // Menu Break
    if (!isContinue)
    {
        break;
    }
    Console.WriteLine("0>Exit");
    Console.WriteLine($"1> Add Contact");
    Console.WriteLine($"2> Display All Contact");
    Console.WriteLine($"3> filter by city");
    Console.WriteLine($"4>update contactname");
    Console.WriteLine($"5>Delete contactname");

    int choice = int.Parse(Console.ReadLine());


    switch (choice)
    {
        case 1:
            Console.WriteLine("Enter Name");
            contact.Name = Console.ReadLine();
            Console.WriteLine("Enter Address");
            contact.Address = Console.ReadLine();
            Console.WriteLine("Enter City");
            contact.City = Console.ReadLine();
            Console.WriteLine("Enter PhoneNumber");
            contact.PhoneNumber = long.Parse(Console.ReadLine());
            try
            {
                Console.WriteLine(contactrepo.AddContact(contact));
            }
            catch (ContactAllreadyExistException caex)
            {
                Console.WriteLine(caex.Message);
            }
            break;

        #region Display All product
        case 2:
            List<Contact> getallproduct = contactrepo.GetAllProduct();
            foreach (Contact item in getallproduct)
            {
                Console.WriteLine(item);
            }
            break;
        #endregion
        case 3:
            Console.WriteLine("Enter City");
            string city = Console.ReadLine();
            List<Contact>result=contactrepo.GetContactByCity(city);
            foreach(Contact item in result)
            {
                Console.WriteLine(item);
            }
            break;

        case 4:
            Console.WriteLine("Enter name tobe Updated");
            string ContactName = Console.ReadLine();
            contactrepo.GetContactByName(ContactName);
            Console.WriteLine("Enter New Contact Name");
            string NewConactName = Console.ReadLine();
            Console.WriteLine(contactrepo.UpdateContact(ContactName, NewConactName));
            List<Contact> getallproduct1 = contactrepo.GetAllProduct();
            foreach (Contact item in getallproduct1)
            {
                Console.WriteLine(item);
            }
            break;
        case 5:
            Console.WriteLine("Enter Contact name To Be deleted");
            var ContactToBeDelted = Console.ReadLine();
            Console.WriteLine(contactrepo.DeleteContactByname(ContactToBeDelted));
            List<Contact> getallproduct2 = contactrepo.GetAllProduct();
            foreach (Contact item in getallproduct2)
            {
                Console.WriteLine(item);
            }
            break;

        case 0:
            isContinue = false;
            break;
        
        default:
            break;

    }
}

   

