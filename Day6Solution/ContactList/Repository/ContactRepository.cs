﻿using ContactList.Exception;
using ContactList.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactList.Repository
{
    

    internal class ContactRepository
    {
        List<Contact> contactlist;
        public ContactRepository()
        {
            contactlist= new List<Contact>()
            {
                new Contact(){ Name = "Triveni",Address="Chembur",City="Mumbai",PhoneNumber = 8850596830},
                new Contact(){ Name = "Sharyu",Address="Kurla",City="Mumbai",PhoneNumber = 7715015172},
            };
        }

        public string AddContact(Contact contact)
        {
            var contactExist = GetContactByName(contact.Name);
            if (contactExist == null)
            {
                contactlist.Add(contact);
                return $"Contact Added Successfully";
            }
            else
            {
                throw new ContactAllreadyExistException($"{contact.Name} already Exist");
            }


        }

        public List<Contact> GetAllProduct()
        {
            return contactlist;
        }
        public List<Contact> GetContactByCity(String city)
        {
            return contactlist.Where(p => p.City == city).ToList();
            
        }
        public Contact GetContactByName(String name)
        {
            return contactlist.Find(p=>p.Name==name);
        }
        public string UpdateContact(string name, string NewValue)
        {
            var contact = GetContactByName(name);
            if (contact != null)
            {
                contact.Name = NewValue;
                return "Update Successfully";
            }
            else
            {
                return "not updated";
            }

        }

        public string DeleteContactByname(string name)
        {
            var contact = GetContactByName(name);
            if(contact != null)
            {
                contactlist.Remove(contact);
                return "Deleted successfully";
            }
            else
            {
                return "Contact name you want delete is not exist";
            }
        }

    }
}
