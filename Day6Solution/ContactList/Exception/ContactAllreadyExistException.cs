﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactList.Exception
{
    internal class ContactAllreadyExistException : ApplicationException
    {
        public ContactAllreadyExistException(string? message) : base(message)
        {
        }
    }
}
