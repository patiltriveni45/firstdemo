﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionList.Exceptions
{
    internal class ProductAllreadyExistException : ApplicationException
    {
        public ProductAllreadyExistException(string? message) : base(message)
        {
        }
    }
}
