﻿// See https://aka.ms/new-console-template for more information

using CollectionList.Exceptions;
using CollectionList.Model;
using CollectionList.Repository;

ProductRepository productrepo = new ProductRepository();


//Get ALl Produtct
Console.WriteLine("Display ALl Product");
List<Product> productlist=productrepo.GetAllProduct();
foreach(Product item in productlist)
Console.WriteLine(item);

//Add Product in The list

Console.WriteLine("Product List After Addition of Item");
Product product = new Product() { Id = 3, Name = "Mobile", Price = 45000 };
try
{
    Console.WriteLine(productrepo.AddProduct(product));
    foreach(Product item in productlist)
    {
        Console.WriteLine(item);
    }
}
catch(ProductAllreadyExistException paex)
{
    Console.WriteLine(paex.Message);
}

Console.WriteLine("Deletion Operation");


