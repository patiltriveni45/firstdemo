﻿using CollectionList.Exceptions;
using CollectionList.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionList.Repository
{
    internal class ProductRepository
    {
        //Declare list
        List<Product> products;
        public ProductRepository()
        {
            products = new List<Product>()
            {
                new Product() { Id = 1, Name = "LG Tv", Price = 20000 },
                new Product() { Id = 2, Name = "laptop", Price = 40000 },
            };
            
        }

        //Get Product List
        public List<Product> GetAllProduct()
        {
            return products;
        }

        //Add Product in list

        public string AddProduct(Product product)
        {
            //checked Whether the given product exist in given list or not
            var productExist = GetProductByName(product.Name);
            if(productExist == null)
            {
                products.Add(product);
                return $"Product Added Successfully";
            }
            else
            {
                throw new ProductAllreadyExistException($"{product.Name} already Exist");
            }

        }
        private Product GetProductByName(String name)
        {
            return products.Find(p=>p.Name == name);
        }
        
        //delete Product
        public bool DeleteProductByname(string name)
        {
            var product = GetProductByName(name);
            return product != null ?products.Remove(product):false;
        }
    }
}
