﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day6_Task.Model
{
    internal class Contact
    {
        public string Name { get; set; }    
        public string Address { get; set; }
        public string City { get; set; }
        public long PhoneNumber { get; set; }

        public override string ToString()
        {
            return $"Name::{Name}\t Address::{Address}\t City::{City}\tPhoneNumber::{PhoneNumber}";
        }
    }
}
