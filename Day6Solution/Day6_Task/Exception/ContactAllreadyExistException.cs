﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day6_Task.Exception
{
    internal class ContactAllreadyExistException:ApplicationException
    {
        public ContactAllreadyExistException(string? message) : base(message)
        {

        }
    }
}
