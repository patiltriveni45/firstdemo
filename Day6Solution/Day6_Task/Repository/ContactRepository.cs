﻿using Day6_Task.Exception;
using Day6_Task.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day6_Task.Repository
{
    internal class ContactRepository
    {
        List<Contact> contacts;
        public ContactRepository()
        {
            contacts = new List<Contact>()
            {
                new Contact(){ Name = "Triveni",Address="Chembur",City="Mumbai",PhoneNumber = 8850596830},
                new Contact(){ Name = "Shreya",Address="Panvel",City="Mumbai",PhoneNumber = 7715015172},
            };
        }
        public List<Contact> GetAllContact()
        {
            return contacts;
        }

        public string AddContact(Contact contact)
        {
            //checked Whether the given Contact exist in given list or not
            var contactExist = GetContactByName(contact.Name);
            if (contactExist == null)
            {
                contacts.Add(contact);
                return $"Contact Added Successfully";
            }
            else
            {
                throw new ContactAllreadyExistException($"{contact.Name} already Exist");
            }

        }
        private Contact GetContactByName(String name)
        {
            return contacts.Find(p => p.Name == name);
        }
        public bool DeleteContactByname(string name)
        {
            var contact = GetContactByName(name);
            return contact != null ? contacts.Remove(contact) : false;
        }

        public string UpdateContact(string name,string NewValue)
        {
            var contact = GetContactByName(name);
            if (contact == null)
            {
                
                return "Contact name not exist";
            }
            else
            {
                contact.Name = NewValue;
                return "Update Successfully";
            }
            
        }
        

    }
}
