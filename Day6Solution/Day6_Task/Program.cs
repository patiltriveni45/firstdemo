﻿// See https://aka.ms/new-console-template for more information


using Day6_Task.Model;
using Day6_Task.Repository;
using Day6_Task.Exception;
Console.WriteLine("Contact Appliaction Using List");

Console.WriteLine("Display All Product");

ContactRepository contactrepo = new ContactRepository();
List<Contact> contactList = contactrepo.GetAllContact();
foreach(Contact item in contactList)
Console.WriteLine(item);


Contact contact = new Contact();
contact.Name = Console.ReadLine();
contact.Address= Console.ReadLine();
contact.City = Console.ReadLine();
contact.PhoneNumber = int.Parse(Console.ReadLine());

//Contact contact = new Contact() {Name = "Mrunali", Address = "Panvel",City="Mumbai",PhoneNumber=8692036247 };
try
{
    Console.WriteLine(contactrepo.AddContact(contact));
    foreach (Contact item in contactList)
    {
        Console.WriteLine(item);
    }
}
catch (ContactAllreadyExistException caex)
{
    Console.WriteLine(caex.Message);
}

//Delete Contact

Console.WriteLine("Deletion Operation:");
Console.WriteLine("Enter Contact name To Be deleted");
var ContactToBeDelted = Console.ReadLine();
Console.WriteLine(contactrepo.DeleteContactByname(ContactToBeDelted));
foreach(Contact item in contactList)
{
    Console.WriteLine(item);
}


//Update Contact
Console.WriteLine("Updation Operation");
Console.WriteLine("Enter name tobe Updated");
string ContactName = Console.ReadLine();
Console.WriteLine("Enter New Contact Name");
string NewConactName = Console.ReadLine();
Console.WriteLine(contactrepo.UpdateContact(ContactName, NewConactName));
foreach (Contact item in contactList)
{
    Console.WriteLine(item);
}